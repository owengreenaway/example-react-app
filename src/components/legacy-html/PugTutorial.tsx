import * as React from "react";
import { Link } from "react-router-dom";
import ScrollToTopOnMount from "../../app/ScrollToTopOnMount";
import Container from "../../ui/container/Container";

const PugTutorial = ({}) => {
  return (
    <Container>
      <ScrollToTopOnMount />
      <header className="header">
        <Link to="/">
          <h2>Home</h2>
        </Link>
      </header>
      <p>
        I am assuming you have Jade set up already and it is compiling using
        grunt-contrib-jade in a gruntfile.js.
      </p>
      <p>
        Start with a simple json file. I'll call mine data.json and save it in
        the same directory as the gruntfile.
      </p>
      <code title="data.json">
        <pre>
          {`
            {
              "name": "Owen",
              "description": "Rendering json isn't <b>too</b> hard."
            }
          `}
        </pre>
      </code>
      <p>
        You probably only need to add one line to your gruntfile to tell it
        where the json file is. Mine looks like this:
      </p>
      <code title="gruntfile.js">
        <pre>
          {`
            // Compile the Jade into HTML
            jade: {
                compile: {
                    options: {
                        pretty: false,
                        data: grunt.file.readJSON("data.json")
                    },
                    files: [{
                        expand: true,
                        cwd: "",
                        src: ["*.jade"],
                        dest: "demo",
                        ext: ".html"
                    }]
                }
            },
          `}
        </pre>
      </code>
      <p>
        We can now reference any key in the first layer. For example the jade:
      </p>
      <code>
        <pre>
          {`
            p My name is #{name}
          `}
        </pre>
      </code>
      <p>Renders to:</p>
      <p>My name is Owen</p>
      <p>Yay, magic!</p>
      <p>Now we try the description</p>
      <code>
        <pre>
          {`
            p My name is #{name}
            p #{description}
          `}
        </pre>
      </code>
      <div>
        Renders to:
        <p>My name is Owen</p>
        <p>Rendering json isn't {`<b>too</b>`} hard.</p>
      </div>
      <p>
        If you're me you then spend a few hours mucking about looking for a
        tutorial. Whereas you'll know to try:
      </p>
      <code>
        <pre>
          {`
            p My name is #{name}
            p !{description}
          `}
        </pre>
      </code>
      <div>
        Renders to:
        <p>My name is Owen</p>
        <p>
          Rendering json isn't <b>too </b>hard.
        </p>
      </div>
      <p>Awesome!</p>
      <p>
        Be careful with your json file. You might want to put new lines in your
        json values to help you read the json but that causes a "Unable to
        parse" error.
      </p>
      <p>
        Final point: When making this tutorial I didn't want my own jade to
        render "{`#{description}`}" for example so I have to use{" "}
        <a href="http://www.ascii.cl/htmlcodes.htm">html codes</a> such as "{`&num;`}"
      </p>
    </Container>
  );
};

export default PugTutorial;
