import * as React from "react";
import { Link } from "react-router-dom";
import ScrollToTopOnMount from "../../app/ScrollToTopOnMount";
import Container from "../../ui/container/Container";
import BaseTree from "./base-tree/BaseTree";
import { createData } from "./createData";
import { LazyLoad } from "./lazy-loading/LazyLoading";
import { RenderTimer } from "./render-timer/RenderTimer";
import {
  StaticCss,
  StyledIndent,
  styledPropRenderFunction,
  StyledRow,
  unStyledIndent,
  unStyledPropRenderFunction
} from "./ui/ui";

// tslint:disable:jsx-no-lambda

type RouteType =
  | "NONE"
  | "RECURIVE_EMOTION"
  | "RECURIVE_CSS"
  | "LIST_EMOTION"
  | "LIST_CSS"
  | "LAZY_CSS";

interface PerformanceTestingState {
  currentDemo: RouteType;
  bigArray: any;
  bigNestedObject: any;
  max: number;
  amountOfDepth: string;
}

class PerformanceTesting extends React.Component<{}, PerformanceTestingState> {
  timestamp = new Date();

  constructor(props: {}) {
    super(props);
    const data = createData(1000, "medium");

    this.state = {
      amountOfDepth: "medium",
      bigArray: data.bigArray,
      bigNestedObject: data.bigNestedObject,
      currentDemo: "NONE" as RouteType,
      max: 1000
    };
  }

  startDemo = (name: RouteType) => {
    this.timestamp = new Date();
    this.setState({ currentDemo: name });
  };

  createMoreData = () => {
    const data = createData(this.state.max, this.state.amountOfDepth);
    this.setState({
      bigArray: data.bigArray,
      bigNestedObject: data.bigNestedObject
    });
  };

  render() {
    const {
      bigArray,
      bigNestedObject,
      currentDemo,
      amountOfDepth
    } = this.state;

    return (
      <>
        <Container>
          <ScrollToTopOnMount />
          <header className="header">
            <Link to="/">
              <h2>Home</h2>
            </Link>
          </header>
          <p>
            Note: the first run of your first experiment you run will be slower
            than all subsequent runs
          </p>
          <p>Current demo: {this.state.currentDemo}</p>

          <label>
            Create a dataset of approximate size:{" "}
            <input
              type="string"
              onChange={e =>
                this.setState({ max: parseInt(e.target.value, 10) })
              }
              value={this.state.max}
            />
          </label>
          <button onClick={this.createMoreData}>Create</button>
          <p>Current data has a length of {bigArray.length}</p>

          <label>
            <input
              type="radio"
              checked={amountOfDepth === "shallow"}
              onChange={() =>
                this.setState({ amountOfDepth: "shallow" }, this.createMoreData)
              }
            />
            Shallow depth
          </label>

          <label>
            <input
              type="radio"
              checked={amountOfDepth === "medium"}
              onChange={() =>
                this.setState({ amountOfDepth: "medium" }, this.createMoreData)
              }
            />
            Medium depth
          </label>

          {/* <label>
            <input
              type="radio"
              checked={amountOfDepth === "deep"}
              onChange={() => this.setState({ amountOfDepth: "deep" })}
            />
            Deep depth
          </label> */}

          {currentDemo === "NONE" ? (
            <ul>
              <li>
                <button onClick={() => this.startDemo("RECURIVE_EMOTION")}>
                  Recursive nesting with Emotion
                </button>
              </li>
              <li>
                <button onClick={() => this.startDemo("RECURIVE_CSS")}>
                  Recursive nesting without Emotion
                </button>
              </li>
              <li>
                <button onClick={() => this.startDemo("LIST_EMOTION")}>
                  List with Emotion
                </button>
              </li>
              <li>
                <button onClick={() => this.startDemo("LIST_CSS")}>
                  List without Emotion
                </button>
              </li>
              <li>
                <button onClick={() => this.startDemo("LAZY_CSS")}>
                  Lazy loading without Emotion
                </button>
              </li>
              {/* <li>Report</li> */}
            </ul>
          ) : (
            <ul>
              <li>
                <button onClick={() => this.startDemo("NONE")}>
                  Don't render anything
                </button>
              </li>
            </ul>
          )}
        </Container>

        {currentDemo === "NONE" ? (
          <div
            style={{
              height: "9999px"
            }}
          />
        ) : (
          <RenderTimer startTime={this.timestamp} />
        )}

        {currentDemo === "RECURIVE_EMOTION" ? (
          <BaseTree
            name={bigNestedObject.name}
            dependencies={bigNestedObject.dependencies}
            row={styledPropRenderFunction}
            Indent={StyledIndent}
          />
        ) : null}

        {currentDemo === "RECURIVE_CSS" ? (
          <StaticCss>
            <BaseTree
              name={bigNestedObject.name}
              dependencies={bigNestedObject.dependencies}
              row={unStyledPropRenderFunction}
              Indent={unStyledIndent}
            />
          </StaticCss>
        ) : null}

        {currentDemo === "LIST_EMOTION"
          ? bigArray.map((item: any) => (
              <div
                key={item.key}
                style={{
                  background: "grey",
                  paddingLeft: item.formattedDepth
                }}
              >
                <StyledRow>{item.name}</StyledRow>
              </div>
            ))
          : null}

        {currentDemo === "LIST_CSS" ? (
          <StaticCss>
            {bigArray.map((item: any) => (
              <div
                key={item.key}
                style={{
                  background: "grey",
                  paddingLeft: item.formattedDepth
                }}
              >
                <div className="unStyledRow">{item.name}</div>
              </div>
            ))}
          </StaticCss>
        ) : null}

        {currentDemo === "LAZY_CSS" ? (
          <StaticCss>
            <LazyLoad bigArray={bigArray} timestamp={this.timestamp} />
          </StaticCss>
        ) : null}
      </>
    );
  }
}

export default PerformanceTesting;
