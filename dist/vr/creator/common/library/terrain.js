AFRAME.registerComponent("terrain", {
  schema: {
    verticeHeights: {
      type: "array",
    },
    width: {
      type: "number",
    },
    density: {
      type: "number",
    },
    src: {
      type: "string",
    },
  },
  init: function () {
    const geometry = new THREE.PlaneGeometry(
      this.data.width,
      this.data.width,
      this.data.density,
      this.data.density
    );
    for (var i = 0, l = geometry.vertices.length; i < l; i++) {
      geometry.vertices[i].z = parseFloat(this.data.verticeHeights[i]);
    }
    // The Aframe component is positioned at -1
    geometry.translate(0, 0, 1);
    const material = this.el.components.material.material;
    const mesh = new THREE.Mesh(geometry, material);
    this.el.setObject3D("mesh", mesh);
  },
});
