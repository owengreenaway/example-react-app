import * as React from "react";
import { Link } from "react-router-dom";
import ScrollToTopOnMount from "../../app/ScrollToTopOnMount";
import Container from "../../ui/container/Container";
import { List } from "../../ui/list/List";
import { Subtitle } from "../../ui/subtitle/Subtitle";
import { Title } from "../../ui/title/Title";

const StyleGuide = ({}) => {
  return (
    <Container>
      <ScrollToTopOnMount />
      <header className="header">
        <Link to="/">
          <h2>Home</h2>
        </Link>
      </header>

      <section>
        <Title copy="Update" />
        <p>
          This was written in 2017. I now use Prettier to automatically enforce
          consistent styles.
        </p>
      </section>

      <section>
        <Title copy="Research and credits" />
        <p>
          After reading all the style guides referenced in this{" "}
          <a href="https://css-tricks.com/css-style-guides/">
            css-tricks article
          </a>{" "}
          I have created my own style guide.
        </p>
      </section>

      <section id="all">
        <Title copy="For all languages" />
        <List>
          <li>Use 2 spaces for indentation.</li>
          <li>Build mobile first.</li>
          <li>Commit when each section is complete.</li>
          <li>Write useful commit messages.</li>
          <li>Use lower case for nearly everything.</li>
        </List>
      </section>

      <section id="react">
        <Title copy="React.js" />
        <List>
          <li>
            I agree with{" "}
            <a href="https://github.com/airbnb/javascript/tree/master/react">
              airbnb
            </a>{" "}
            on their choices. I use their rules with Eslint with only minor
            changes.
          </li>
          <li>
            Use tertiary with null instead of "&amp;&amp;" to reduce cognitive
            load.
          </li>
        </List>
      </section>

      <section id="html">
        <Title copy="HTML Preprocessors" />
        <List>
          <li>
            Paragraphs of text should always be placed in a p tag. Never use
            multiple br tags.
          </li>
          <li>Use multiple single-line comments.</li>
          <li>Always put quotes around attributes for readability.</li>
          <li>Use a master template.</li>
          <li>Use mixins.</li>
        </List>
      </section>

      <section id="scss">
        <Title copy="SCSS" />
        <List>
          <li>Where possible only style classes instead of tags.</li>
          <li>Avoid absolute positioning. Only use when necessary.</li>
          <li>Only use !important in extremely rare situations.</li>
          <li>
            Put breakpoints in a style declaration (scss way instead of css
            way).
          </li>
          <li>Avoid any breakpoint apart from @include breakpoint(sm){}.</li>
          <li>Use rem font sizes.</li>
          <li>All text is in the HTML so it can be in the CMS.</li>
          <li>All colours are variables.</li>
          <li>Use CSS for circular and rectangular shapes.</li>
          <li>
            Use the smallest z-index needed. Reorder HTML markup instead of
            using z-index.
          </li>
          <li>Use {`//`} for comment blocks.</li>
          <li>Avoid specifying units for zero values.</li>
          <li>
            Avoid shorthand declarations unless you actually want to set
            everything.
          </li>
        </List>
        <Subtitle copy="File Organisation and Naming" />
        <List>
          <li>
            Never style global tags apart from in the structure.scss file.
          </li>
          <li>Import all files into a main.scss.</li>
          <li>All variables in one file.</li>
          <li>All z-index settings are in one file.</li>
          <li>Create a new SCSS file for each component.</li>
          <li>BEM styling for naming.</li>
          <li>Keep a flat structure for children and grandchildren.</li>
          <li>Every class contains the block name.</li>
          <li>Multiword block names are in camelScript.</li>
          <li>Use &amp;__item and &amp;--modifier.</li>
          <li>
            Use additional –modifier classes instead of overwriting variables.
          </li>
          <li>Use our mixins and helper classes.</li>
          <li>Typography is all contained in one file.</li>
          <li>
            Use global layout classes instead of repeating layout inside each
            component.
          </li>
        </List>
        <Subtitle copy="Spacing and ordering" />
        <List>
          <li>Always include ; in property declarations.</li>
          <li>Put spaces after : in property declarations.</li>
          <li>Put spaces before brackets in rule declarations.</li>
          <li>Put line breaks between rulesets.</li>
          <li>
            When grouping selectors, keep individual selectors to a single line.
          </li>
          <li>Place closing braces of declaration blocks on a new line.</li>
          <li>Each declaration should appear on its own line.</li>
          <li>Leave a blank line at the end of the document.</li>
          <li>Arrange property declarations in alphabetically order.</li>
        </List>
        <Subtitle copy="Example" />
        <code>
          <pre>{`.classnameExample {
	// mobile version of classnameExample
	@include span(12);
	height: 300px;

	// desktop version of classnameExample
	@include breakpoint(sm){
		@include span(6);

		&:last-of-type {
			@include last;
		}
	}

	// Modifiers
	&--light {
		background-color: $white;
		color: $black;
	}

	// Parent wrapper
	&__wrapper {
		background: $primary;
	}

	// Children and grandchildren in order of HTML
	&__inner {
		@include span(12);
		position: relative;
	}

	&__logo {
		position: absolute;
		bottom: 0;
		right: 0;
	}
}
`}</pre>
        </code>
      </section>

      <section id="javascript">
        <Title copy="JavaScript" />
        <List>
          <li>Use semi-colons.</li>
          <li>Put brackets around if statements.</li>
        </List>
      </section>

      <section id="accessibility">
        <Title copy="Accessibility" />
        <Subtitle copy="Always use:" />
        <List>
          <li>
            Every button and link needs a focus and hover effect and additional
            text content so it can be understood out of context.
          </li>
          <li>Every input has a label.</li>
          <li>All images have alt tags.</li>
          <li>All text has a high contrast ratio at all times.</li>
          <li>All text on images have a filter.</li>
          <li>All text is larger than 12px.</li>
          <li>Skip to content hidden link.</li>
          <li>Aria states on all state changes.</li>
          <li>Set tabindex automatically.</li>
        </List>
        <Subtitle copy="Try to use:" />
        <List>
          <li>Use semantic tags [header, footer, main, nav].</li>
          <li>Use roles (search).</li>
          <li>Use aria-alerts.</li>
          <li>JavaScript fallbacks.</li>
        </List>
      </section>
    </Container>
  );
};

export default StyleGuide;
