AFRAME.registerComponent("paint-terrain", {
  schema: {
    colors: {
      type: "array",
    },
    length: {
      type: "number",
    },
  },
  init: function () {
    this.canvas = document.getElementById("terrainTexture");
    this.ctx = this.canvas.getContext("2d");
    this.canvas.width = 2024;
    this.canvas.height = 2024;

    this.normalCanvas = document.createElement("canvas");
    this.normalCtx = this.normalCanvas.getContext("2d");
    this.normalCanvas.width = 2024;
    this.normalCanvas.height = 2024;

    const paintColorLayer = (index) => {
      var layerCanvas = document.createElement("canvas");
      layerCanvas.width = 2024;
      layerCanvas.height = 2024;
      const layerContext = layerCanvas.getContext("2d");
      const mask = document.getElementById(`mask${index}`);
      layerContext.drawImage(mask, 0, 0, 2024, 2024);
      layerContext.globalCompositeOperation = "source-in";

      const color = this.data.colors[index];
      if (color) {
        layerContext.fillStyle = color;
        layerContext.fillRect(0, 0, layerCanvas.height, layerCanvas.width);
      }

      const texture = document.getElementById(`texture${index}`);
      if (texture) {
        const pattern = layerContext.createPattern(texture, "repeat");
        layerContext.fillStyle = pattern;
        // layerContext.fillStyle = "green";
        layerContext.fillRect(0, 0, layerCanvas.height, layerCanvas.width);
      }

      this.ctx.drawImage(layerCanvas, 0, 0);
    };

    const paintNormalLayer = (index) => {
      var layerCanvas = document.createElement("canvas");
      layerCanvas.width = 2024;
      layerCanvas.height = 2024;
      const layerContext = layerCanvas.getContext("2d");
      const mask = document.getElementById(`mask${index}`);
      layerContext.drawImage(mask, 0, 0, 2024, 2024);
      layerContext.globalCompositeOperation = "source-in";

      const normalTexture = document.getElementById(`normal${index}`);
      if (normalTexture) {
        const pattern = layerContext.createPattern(normalTexture, "repeat");
        layerContext.fillStyle = pattern;
        layerContext.fillRect(0, 0, layerCanvas.height, layerCanvas.width);
      } else {
        const FLAT_NORMAL_MAP = "rgba(126, 132, 255, 1)";
        layerContext.fillStyle = FLAT_NORMAL_MAP;
        layerContext.fillRect(0, 0, layerCanvas.height, layerCanvas.width);
      }
      this.normalCtx.drawImage(layerCanvas, 0, 0);
    };

    for (let i = 0; i < this.data.length; i++) {
      paintColorLayer(i);
      paintNormalLayer(i);
    }

    document.getElementById(
      "terrainNormal"
    ).src = this.normalCanvas.toDataURL();

    const material = this.el.getObject3D("mesh").material;
    if (material.map) {
      material.map.needsUpdate = true;
    }
  },
});
