import * as React from "react";
import { Link } from "react-router-dom";
import ScrollToTopOnMount from "../../app/ScrollToTopOnMount";
import Container from "../../ui/container/Container";

const Python = ({}) => {
  return (
    <Container>
      <ScrollToTopOnMount />
      <header className="header">
        <Link to="/">
          <h2>Home</h2>
        </Link>
      </header>
      <p>
        I taught my students how to create and modify this object oriented
        Python 3 game. Turtle was the only pre-built module we needed to use.
        Hosted on Trinket.io
      </p>
      <iframe
        src="https://trinket.io/embed/python/9f154be1a0"
        width="100%"
        height="700px"
        frameBorder={0}
        allowFullScreen={true}
      />
    </Container>
  );
};

export default Python;
