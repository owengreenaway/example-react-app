import * as React from "react";
import styled from "react-emotion";

const Wrapper = styled("h3")`
  font-size: 1.3rem;
  margin: 2.5rem 0;
  @media (min-width: 768px) {
    font-size: 1.5rem;
  }
`;

export interface Props {
  copy: string;
}

export const Subtitle: React.SFC<Props> = ({ copy }) => {
  return <Wrapper>{copy}</Wrapper>;
};
