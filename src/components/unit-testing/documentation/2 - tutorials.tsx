import { shallow } from "enzyme";
import * as React from "react";
import * as renderer from "react-test-renderer";
import HelloWorld from "../../../ui/hello-world/HelloWorld";

describe("Given the HelloWorld component", () => {
  it("then the message is rendered", () => {
    const testString = "test-string";
    const wrapper = shallow(<HelloWorld message={testString} />);
    const message = wrapper.find(`[data-testing-id="hello-world-message"]`);
    expect(message.props().children).toBe(testString);
  });

  describe("when there are children", () => {
    it("then the children are rendered", () => {
      const wrapper = shallow(
        <HelloWorld message={"test-string"}>
          <p>I am the child</p>
        </HelloWorld>
      );
      const childrenWrapper = wrapper.find(
        `[data-testing-id="hello-world-children"]`
      );
      expect(childrenWrapper.exists()).toBe(true);
      expect(childrenWrapper.contains(<p>I am the child</p>)).toBe(true);
    });

    it("then it matches the snapshot", () => {
      const snapshot = renderer
        .create(
          <HelloWorld message={"test-string"}>
            <p>I am the child</p>
          </HelloWorld>
        )
        .toJSON();
      expect(snapshot).toMatchSnapshot();
    });
  });

  describe("when there are no children", () => {
    it("then the children are not rendered", () => {
      const wrapper = shallow(<HelloWorld message={"test-string"} />);
      const childrenWrapper = wrapper.find(
        `[data-testing-id="hello-world-children"]`
      );
      expect(childrenWrapper.exists()).toBe(false);
    });

    it("then it matches the snapshot", () => {
      const snapshot = renderer
        .create(<HelloWorld message={"test-string"} />)
        .toJSON();
      expect(snapshot).toMatchSnapshot();
    });
  });
});
