import * as React from "react";
import styled from "react-emotion";

export const StyledIndent = styled("section")`
  padding-left: 7.5px;
  background: grey;
`;

export const StyledRow = styled("section")`
  border: 1px solid black;
  background: white;
  padding: 5px 15px;
`;

export const GoodStyledRow = styled("section")`
  border: 1px solid green;
  background: lightgreen;
  padding: 5px 15px;
`;

export const BadStyledRow = styled("section")`
  border: 1px solid red;
  background: lightpink;
  padding: 5px 15px;
`;

export const StaticCss = styled("section")`
  .unStyledRow {
    border: 1px solid black;
    background: white;
    padding: 7.5px 15px;
  }

  .unStyledIndent {
    padding-left: 10px;
    background: grey;
  }
`;

export const styledPropRenderFunction = (props: any) => {
  if (props.type === "good") {
    return <GoodStyledRow>Selected: {props.name}</GoodStyledRow>;
  }
  if (props.type === "bad") {
    return <BadStyledRow>Selected: {props.name}</BadStyledRow>;
  }
  return <StyledRow>{props.name}</StyledRow>;
};

export const unStyledPropRenderFunction = (props: any) => {
  return <div className="unStyledRow">{props.name}</div>;
};

export const unStyledIndent = (props: any) => {
  return <section className="unStyledIndent">{props.children}</section>;
};
