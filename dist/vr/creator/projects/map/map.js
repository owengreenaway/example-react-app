const points = {
  handBySide: {
    x: 0.2,
    y: 1,
  },
  ireland: {
    x: -0.04,
    y: 1.17,
  },
  dublin: {
    x: 0.099,
    y: 1.188,
  },
  airport: {
    x: -0.057,
    y: 1.47,
  },
  city: {
    x: -0.104,
    y: 1.129,
  },
};

const flash = [
  {
    color: "#308025",
    duration: 1,
  },
  {
    color: "#308025",
    duration: 198,
  },
  {
    color: "#666",
    duration: 1,
  },
];

const durations = {
  irelandClick: 1000,
  dublinClick: 1000,
  airportClick: 1000,
  cityClick: 1000,
  handBySideClick: 1000,
  grabGlobe: 1000,
};

const data = {
  settings: {
    animation: {
      totalDuration: 8000,
    },
    hands: {
      showHands: false,
    },
    scene: {
      position: "0 0 -1.5",
      animation: "from: 0 -50 0; to: 0 50 0; dur: 4000; property: rotation; loop: true; dir: alternate; easing: easeInOutQuad"
    },
  },
  models: [
    {
      title: "map-global",
      position: "0 1.2 -0.35",
      visible_animations: [
        // Bug when the whole sequence repeats the initial value isn't restored
        {
          visible: true,
          duration: 1,
        },
        {
          visible: false,
          duration: durations.irelandClick,
        },
      ],
      primatives: [
        {
          type: "image",
          src: "media/map-global.png",
          color: "#666",
        },
      ],
    },
    {
      title: "map-ireland",
      position: "0 1.2 -0.35",
      visible_animations: [
        {
          visible: false,
          duration: 1,
        },
        {
          visible: true,
          duration: durations.irelandClick,
        },
        {
          visible: false,
          duration: durations.dublinClick,
        },
      ],
      primatives: [
        {
          type: "image",
          src: "media/map-ireland.png",
          color: "#666",
        },
      ],
    },
    {
      title: "map-dublin",
      position: "0 1.2 -0.35",
      visible_animations: [
        {
          visible: false,
          duration: 1,
        },
        {
          visible: true,
          duration: durations.irelandClick + durations.dublinClick,
        },
      ],
      primatives: [
        {
          type: "image",
          src: "media/map-dublin.png",
          color: "#666",
        },
      ],
    },
    {
      title: "map-dublin-directions",
      position: "0 1.2 -0.35",
      visible_animations: [
        {
          visible: false,
          duration: 1,
        },
        {
          visible: true,
          duration:
            durations.irelandClick +
            durations.dublinClick +
            durations.airportClick +
            durations.cityClick,
        },
      ],
      primatives: [
        {
          type: "image",
          src: "media/map-dublin-directions.png",
          color: "#666",
        },
      ],
    },
    {
      title: "head",
      position: "0 1.7 0.35",
      lookAt: "#marker",
      primatives: [
        {
          title: "head",
          type: "sphere",
          radius: "0.15",
          color: "#666",
        },
        {
          title: "visor",
          type: "box",
          position: "0 0 0.125",
          height: "0.1",
          width: "0.30",
          depth: "0.1",
          color: "#308025",
        },
      ],
    },
    {
      title: "right controller",
      position: "0.2 1 0.2",
      position_animations: [
        {
          position: `${points.ireland.x} ${points.ireland.y} 0.2`,
          duration: durations.irelandClick,
          easing: "easeOutCirc",
        },
        {
          position: `${points.dublin.x} ${points.dublin.y} 0.2`,
          duration: durations.dublinClick,
          easing: "easeOutCirc",
        },
        {
          position: `${points.airport.x} ${points.airport.y} 0.2`,
          duration: durations.airportClick,
          easing: "easeOutCirc",
        },
        {
          position: `${points.city.x} ${points.city.y} 0.2`,
          duration: durations.cityClick,
          easing: "easeOutCirc",
        },
        {
          position: `${points.handBySide.x} ${points.handBySide.y} 0.2`,
          duration: durations.handBySideClick,
          easing: "easeOutCirc",
        },
        {
          position: `${points.city.x} ${points.city.y} -0.1`,
          duration: durations.grabGlobe,
          easing: "easeOutCirc",
        },
      ],
      primatives: [
        {
          type: "model",
          scale: "0.01 0.01 0.01",
          src:
            "https://cdn.aframe.io/controllers/magicleap/magicleap-one-controller.glb",
        },
      ],
    },
    // {
    //   title: "left controller",
    //   position: "-0.2 1 0.2",
    //   primatives: [
    //     {
    //       type: "model",
    //       scale: "0.01 0.01 0.01",
    //       src:
    //         "https://cdn.aframe.io/controllers/magicleap/magicleap-one-controller.glb",
    //     },
    //   ],
    // },
    {
      title: "marker",
      position: "0.2 1 -0.35",
      position_animations: [
        {
          position: `${points.ireland.x} ${points.ireland.y} -0.35`,
          duration: durations.irelandClick,
          easing: "easeOutCirc",
        },
        {
          position: `${points.dublin.x} ${points.dublin.y} -0.35`,
          duration: durations.dublinClick,
          easing: "easeOutCirc",
        },
        {
          position: `${points.airport.x} ${points.airport.y} -0.35`,
          duration: durations.airportClick,
          easing: "easeOutCirc",
        },
        {
          position: `${points.city.x} ${points.city.y} -0.35`,
          duration: durations.cityClick,
          easing: "easeOutCirc",
        },
        {
          position: `${points.handBySide.x} ${points.handBySide.y} -0.35`,
          duration: durations.handBySideClick,
          easing: "easeOutCirc",
        },
        {
          position: `${points.city.x} ${points.city.y} -0.35`,
          duration: durations.grabGlobe,
          easing: "easeOutCirc",
        },
      ],
      visible_animations: [
        {
          visible: true,
          duration: 1,
        },
        {
          visible: false,
          duration:
            durations.irelandClick +
            durations.dublinClick +
            durations.airportClick +
            durations.cityClick,
        },
      ],
      primatives: [
        {
          type: "cylinder",
          position: "0 0 0.05",
          rotation: "90 0 0",
          height: "0.1",
          radius: "0.005",
          color: "#666",
          color_animations: [
            {
              color: "#666",
              duration: durations.irelandClick,
            },
            ...flash,
            {
              color: "#666",
              duration: durations.dublinClick - 200,
            },
            ...flash,
            {
              color: "#666",
              duration: durations.airportClick - 200,
            },
            ...flash,
            {
              color: "#666",
              duration: durations.cityClick - 200,
            },
          ],
        },
        {
          type: "sphere",
          position: "0 0 0.125",
          radius: "0.03",
          color: "#666",
          color_animations: [
            {
              color: "#666",
              duration: durations.irelandClick,
            },
            ...flash,
            {
              color: "#666",
              duration: durations.dublinClick - 200,
            },
            ...flash,
            {
              color: "#666",
              duration: durations.airportClick - 200,
            },
            ...flash,
            {
              color: "#666",
              duration: durations.cityClick - 200,
            },
          ],
        },
      ],
    },
    {
      title: "start marker",
      position: `${points.airport.x} ${points.airport.y} -0.35`,
      visible_animations: [
        {
          visible: false,
          duration: 1,
        },
        {
          visible: true,
          duration:
            durations.irelandClick +
            durations.dublinClick +
            durations.airportClick,
        },
      ],
      primatives: [
        {
          type: "cylinder",
          position: "0 0 0.05",
          rotation: "90 0 0",
          height: "0.1",
          radius: "0.005",
          color: "#308025",
        },
        {
          type: "sphere",
          position: "0 0 0.125",
          radius: "0.03",
          color: "#308025",
        },
      ],
    },
    // {
    //   title: "end marker",
    //   position: `${points.city.x} ${points.city.y} -0.35`,
    //   visible: false,
    //   visible_animations: [
    //     {
    //       visible: false,
    //       duration: 1,
    //     },
    //     {
    //       visible: true,
    //       duration:
    //         durations.irelandClick +
    //         durations.dublinClick +
    //         durations.airportClick +
    //         durations.cityClick,
    //     },
    //   ],
    //   primatives: [
    //     {
    //       type: "cylinder",
    //       position: "0 0 0.05",
    //       rotation: "90 0 0",
    //       height: "0.1",
    //       radius: "0.005",
    //       color: "#308025",
    //     },
    //     {
    //       type: "sphere",
    //       position: "0 0 0.125",
    //       radius: "0.03",
    //       color: "#308025",
    //     },
    //   ],
    // },
    {
      title: "globe",
      position: `${points.city.x} ${points.city.y} -0.2`,
      visible: false,
      visible_animations: [
        {
          visible: false,
          duration: 1,
        },
        {
          visible: true,
          duration:
            durations.irelandClick +
            durations.dublinClick +
            durations.airportClick +
            durations.cityClick,
        },
      ],
      scale_animations: [
        {
          scale: "0 0 0",
          duration:
            durations.irelandClick +
            durations.dublinClick +
            durations.airportClick +
            durations.cityClick
        },
        {
          scale: "1 1 1",
          duration: 750,
        },
      ],
      primatives: [
        {
          type: "sphere",
          radius: "0.1",
          src: "media/dublin-360.jpg",
        },
      ],
    },
    {
      title: "360 image",
      primatives: [
        {
          type: "sky",
          src: "media/dublin-360.jpg",
          rotation: "0 -75 0",
          position: "0 0 -1.5",
          visible: false,
          visible_animations: [
            {
              visible: false,
              duration: 1,
            },
            {
              visible: true,
              duration:
                durations.irelandClick +
                durations.dublinClick +
                durations.airportClick +
                durations.cityClick +
                durations.handBySideClick +
                durations.grabGlobe,
            },
          ],
          opacity: 0,
          opacity_animations: [
            {
              opacity: 0,
              duration:
                durations.irelandClick +
                durations.dublinClick +
                durations.airportClick +
                durations.cityClick +
                durations.handBySideClick +
                durations.grabGlobe,
            },
            {
              opacity: 1,
              duration: 300,
            },
          ],
        },
      ],
    },
  ],
};
