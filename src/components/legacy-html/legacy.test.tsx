import { shallow } from "enzyme";
import * as React from "react";
import { MemoryRouter } from "react-router";
import * as renderer from "react-test-renderer";
import Blog from "./Blog";
import Home from "./Home";
import PugTutorial from "./PugTutorial";
import Python from "./Python";
import StyleGuide from "./StyleGuide";

(global as any).window.scrollTo = jest.fn();

describe("Given the Blog component", () => {
  it("then it matches the snapshot", () => {
    const snapshot = renderer
      .create(
        <MemoryRouter>
          <Blog />
        </MemoryRouter>
      )
      .toJSON();
    expect(snapshot).toMatchSnapshot();
  });
});

describe("Given the Home component", () => {
  it("then it matches the snapshot", () => {
    const snapshot = renderer
      .create(
        <MemoryRouter>
          <Home />
        </MemoryRouter>
      )
      .toJSON();
    expect(snapshot).toMatchSnapshot();
  });
});

describe("Given the PugTutorial component", () => {
  it("then it matches the snapshot", () => {
    const snapshot = renderer
      .create(
        <MemoryRouter>
          <PugTutorial />
        </MemoryRouter>
      )
      .toJSON();
    expect(snapshot).toMatchSnapshot();
  });
});

describe("Given the Python component", () => {
  it("then it matches the snapshot", () => {
    const snapshot = renderer
      .create(
        <MemoryRouter>
          <Python />
        </MemoryRouter>
      )
      .toJSON();
    expect(snapshot).toMatchSnapshot();
  });
});

describe("Given the StyleGuide component", () => {
  it("then it matches the snapshot", () => {
    const snapshot = renderer
      .create(
        <MemoryRouter>
          <StyleGuide />
        </MemoryRouter>
      )
      .toJSON();
    expect(snapshot).toMatchSnapshot();
  });
});
