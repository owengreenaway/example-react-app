import * as React from "react";
import styled from "react-emotion";

const Wrapper = styled("ul")`
  padding: 0 2rem;
  line-height: 1.8;
  list-style-type: circle;
`;

export const List: React.SFC<{}> = ({ children }) => {
  return <Wrapper>{children}</Wrapper>;
};
