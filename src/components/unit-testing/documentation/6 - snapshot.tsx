import { shallow, ShallowWrapper } from "enzyme";
import * as React from "react";
import * as renderer from "react-test-renderer";
import HelloWorld, {
  HelloWorldProps
} from "../../../ui/hello-world/HelloWorld";

interface SetupInterface {
  props: HelloWorldProps;
  wrapper: ShallowWrapper<HelloWorldProps>;
}

const setup = (propOverrides?: Partial<HelloWorldProps>): SetupInterface => {
  const finalProps = Object.assign(defaultProps, propOverrides);
  return {
    props: finalProps,
    wrapper: shallow(<HelloWorld {...finalProps} />)
  };
};

const snapshot = (
  propOverrides?: Partial<HelloWorldProps>
): renderer.ReactTestRendererJSON => {
  const finalProps = Object.assign(defaultProps, propOverrides);
  return renderer.create(<HelloWorld {...finalProps} />).toJSON();
};

const defaultProps: HelloWorldProps = {
  message: "test-string"
};

const testChild = <p>I am the child</p>;

describe("Given the HelloWorld component", () => {
  it("then the message is rendered", () => {
    const { wrapper, props } = setup();
    const message = wrapper.find(`[data-testing-id="hello-world-message"]`);
    expect(message.props().children).toBe(props.message);
  });

  describe("when there are children", () => {
    it("then the children are rendered", () => {
      const { wrapper } = setup({ children: testChild });
      const childrenWrapper = wrapper.find(
        `[data-testing-id="hello-world-children"]`
      );
      expect(childrenWrapper.exists()).toBe(true);
      expect(childrenWrapper.contains(testChild)).toBe(true);
    });

    it("then it matches the snapshot", () => {
      expect(snapshot({ children: testChild })).toMatchSnapshot();
    });
  });

  describe("when there are no children", () => {
    it("then the children are not rendered", () => {
      const { wrapper } = setup();
      const childrenWrapper = wrapper.find(
        `[data-testing-id="hello-world-children"]`
      );
      expect(childrenWrapper.exists()).toBe(false);
    });

    it("then it matches the snapshot", () => {
      expect(snapshot()).toMatchSnapshot();
    });
  });
});
