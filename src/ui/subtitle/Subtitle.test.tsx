import { shallow } from "enzyme";
import * as React from "react";
import * as renderer from "react-test-renderer";
import { Subtitle } from "./Subtitle";

const setup = (props?: any) => {
  const defaultPropsOfSetup = {
    copy: "unit test string"
  };
  props = Object.assign(defaultPropsOfSetup, props);
  return {
    snapshot: renderer.create(<Subtitle copy={props.copy} />).toJSON(),
    wrapper: shallow(<Subtitle copy={props.copy} />)
  };
};

describe("Given the Subtitle component", () => {
  describe("when given the prop copy", () => {
    it("then it renders the copy", () => {
      const { wrapper } = setup();
      expect(wrapper.contains("unit test string")).toBeTruthy();
    });
  });

  it("then it matches the snapshot", () => {
    const { snapshot } = setup();
    expect(snapshot).toMatchSnapshot();
  });
});
