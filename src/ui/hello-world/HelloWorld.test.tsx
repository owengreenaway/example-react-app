import {
  createSetup,
  createSnapshot,
  findById
} from "../../../utils/testUtils";
import { defaultProps, testChild } from "./__mocks__/props.mock";
import HelloWorld from "./HelloWorld";

const setup = createSetup(HelloWorld, defaultProps);
const snapshot = createSnapshot(HelloWorld, defaultProps);

const findChildren = findById("hello-world-children");
const findMessage = findById("hello-world-message");

describe("Given the HelloWorld component", () => {
  it("then the message is rendered", () => {
    const { wrapper, props } = setup();
    expect(findMessage(wrapper).props().children).toBe(props.message);
  });

  describe("when there are children", () => {
    it("then the children are rendered", () => {
      const { wrapper } = setup({ children: testChild });

      expect(findChildren(wrapper).exists()).toBe(true);
      expect(findChildren(wrapper).contains(testChild)).toBe(true);
    });

    it("then it matches the snapshot", () => {
      expect(snapshot({ children: testChild })).toMatchSnapshot();
    });
  });

  describe("when there are no children", () => {
    it("then the children are not rendered", () => {
      const { wrapper } = setup({ children: undefined });
      expect(findChildren(wrapper).exists()).toBe(false);
    });

    it("then it matches the snapshot", () => {
      expect(snapshot()).toMatchSnapshot();
    });
  });
});
