const defaults = {
  totalDuration: 4000,
  position: "0 0 -4",
  grey: "#666",
  handStyle: "highPoly",
};

const renderProperty = (name, value) => {
  if (value === "") {
    return `${name}`;
  }
  return value !== undefined ? `${name}="${value}"` : "";
};

const renderProperties = (properties) =>
  Object.keys(properties)
    .map((key) => {
      return renderProperty(key, properties[key]);
    })
    .join("\n");

const renderBooleanAnimations = (name, animations = [], outerIndex) => {
  let delay = 0;
  let previousValue = true;

  return animations
    .map((animation, animationIndex) => {
      const { duration } = animation;
      const value = animation[name];
      delay = delay + duration;
      const result = `animation__${name}_${outerIndex}_${animationIndex}="
      from: ${previousValue};
      to: ${value};
      dur: 1;
      delay: ${delay};
      property: ${name};
      startEvents: startAnimation"
    `;
      previousValue = value;
      return result;
    })
    .join("\n");
};

const legacyRenderAnimations = (
  name,
  initialValue,
  animations = [],
  outerIndex
) => {
  let delay = 0;
  let previousValue = initialValue;

  return animations
    .map((animation, animationIndex) => {
      const { duration, easing } = animation;
      const value = animation[name];
      const result = `animation__${name}_${outerIndex}_${animationIndex}="
      from: ${previousValue};
      to: ${value};
      dur: ${duration};
      delay: ${delay};
      property: ${name};
      startEvents: startAnimation"
      ${easing !== undefined ? `easing: ${easing};` : ""}
    `;
      delay = delay + duration;
      previousValue = value;
      return result;
    })
    .join("\n");
};

const renderKeyframes = (type, initialValue, keyframes = [], outerIndex) => {
  let delay = 0;
  let previousValue = initialValue;

  return keyframes
    .map((keyframe, keyframeIndex) => {
      const { value, duration, easing } = keyframe;
      const durationNumber = parseFloat(duration);
      if (type === "visible") {
        delay = delay + durationNumber;
      }
      const result = `animation__${type}_${outerIndex}_${keyframeIndex}="
      from: ${previousValue};
      to: ${value};
      dur: ${durationNumber};
      delay: ${delay};
      property: ${type};
      startEvents: startAnimation;
      ${easing !== undefined ? `easing: ${easing};` : ""}"
    `;
      if (type !== "visible") {
        delay = delay + durationNumber;
      }
      previousValue = value;
      return result;
    })
    .join("\n");
};

const renderAnimations = (outerObject = {}, outerIndex) => {
  const { animations = [] } = outerObject;
  return animations
    .map(({ type, keyframes }) => {
      return renderKeyframes(type, outerObject[type], keyframes, outerIndex);
    })
    .join("\n");
};

const srcCache = {};

const getOptimisedSrc = (src, modelIndex, primativeIndex) => {
  if (src) {
    if (srcCache[src]) {
      return {
        inCache: true,
        src: srcCache[src],
      };
    } else {
      srcCache[src] = `#${modelIndex}_${primativeIndex}_url`;
      return {
        inCache: false,
        src: srcCache[src],
      };
    }
  }
  return {};
};

const renderPrimatives = (primatives, modelIndex) => {
  return primatives
    .map((primative, primativeIndex) => {
      const {
        animations,
        color_animations = [],
        opacity_animations = [],
        src,
        type = "a-box",
        ...properties
      } = primative;

      const isTerrain = src === "#terrainTexture";
      const optimisedSrc = getOptimisedSrc(src, modelIndex, primativeIndex).src;
      return `
        <${type}
          class="primatives"
          ${src && !isTerrain ? `src="${optimisedSrc}"` : ""}
          ${isTerrain && "src=#terrainTexture"}
          ${renderProperties(properties)}
          ${renderAnimations(primative, primativeIndex)}
        ></${type}>
      `;
    })
    .join("\n");
};

const renderModels = (models) =>
  models
    .map((model, modelIndex) => {
      const {
        animations,
        primatives = [],
        visible_animations = [],
        ...properties
      } = model;
      return `<a-entity
        class="model"
        ${renderProperties(properties)}
        ${renderAnimations(model, modelIndex)}
        ${renderBooleanAnimations("visible", visible_animations, modelIndex)}
    >
      ${renderPrimatives(primatives, modelIndex)}
    </a-entity>`;
    })
    .join("\n");

const renderAssetManagementSystem = (models = []) =>
  models
    .map(({ primatives = [] }, modelIndex) =>
      primatives
        .map(({ src, type }, primativeIndex) => {
          if (src === "#terrainTexture") {
            return "";
          }
          if (src) {
            const inCache = getOptimisedSrc(src, modelIndex, primativeIndex)
              .inCache;
            if (!inCache) {
              if (type === "a-gltf-model") {
                return `<a-asset-item id="${modelIndex}_${primativeIndex}_url" src="${src}"></a-asset-item>`;
              }
              return `<img id="${modelIndex}_${primativeIndex}_url" src="${src}"></img>`;
            }
          }
        })
        .join("")
    )
    .join("");

const renderCamera = (videoSettings = {}) => {
  if (!videoSettings.enabled) {
    return "";
  }
  const properties = Object.keys(videoSettings)
    .map((key) => {
      return videoSettings[key] !== undefined
        ? `${key}: ${videoSettings[key]};`
        : "";
    })
    .join(" ");
  return `<a-camera
  camera-recorder="${properties}; workerPath: ../common/vendor/; name: owenAnimationCreatorGif"
></a-camera>`;
};

const renderTerrainAssets = (terrainPaintAsseets) => {
  const assets = terrainPaintAsseets.map((layer, index) => {
    const texture = `<img id="texture${index}" src="${layer.textureSrc}" crossorigin="anonymous"></img>`;
    const normal = `<img id="normal${index}" src="${layer.normalMap}" crossorigin="anonymous"></img>`;
    return `
      <img id="mask${index}" src="${
      layer.maskDataUrl
    }" crossorigin="anonymous"></img>
      ${layer.textureSrc ? texture : ""}
      ${layer.normalMap ? normal : ""}
    `;
  });
  return `
    <canvas id="terrainTexture" crossorigin="anonymous"></canvas>
    <img id="terrainNormal" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNgYAAAAAMAASsJTYQAAAAASUVORK5CYII=" crossorigin="anonymous"></img>
    ${assets.join("\n")}
    `;
};

const renderScene = (
  domElement,
  models,
  settings,
  videoSettings,
  terrainPaintAsseets
) => {
  const { scene = {}, modelProperties = {} } = settings;

  const isHand = ({ id }) => id === "leftHand" || id === "rightHand";
  const hands = models.filter((model) => isHand(model));
  const otherModels = models.filter((model) => !isHand(model));

  const template = `
  <a-scene
    ${renderProperties(scene)}
  >
    <a-assets timeout="10000">
      ${renderTerrainAssets(terrainPaintAsseets)}
      ${renderAssetManagementSystem(models)}
    </a-assets>
    <a-entity id="models"
      ${renderProperties(modelProperties)}
    >
      ${renderModels(otherModels)}
    </a-entity>
    <a-entity light="type: ambient; intensity: 0.5;"></a-entity>
    <a-entity
      light="type: directional;
      castShadow: true;
      intensity: 0.2;
      shadowCameraTop: 20;
      shadowCameraRight: 20;
      shadowCameraLeft: -20;
      shadowCameraBottom: -20;
      shadowRadius: 0.7"
      position="20 30 -20"
    ></a-entity>
    ${renderCamera(videoSettings)}
    ${renderModels(hands)}
  </a-scene>`;

  domElement.innerHTML = template;
};

const renderAnimation = (
  domElement,
  models,
  settings,
  videoSettings,
  terrainPaintAsseets = []
) => {
  const { animation = {} } = settings;
  renderScene(domElement, models, settings, videoSettings, terrainPaintAsseets);

  AFRAME.registerComponent("loop_animation", {
    init: function () {
      const startAnimation = () => {
        document
          .querySelectorAll(".model, .primatives")
          .forEach((model) => model.emit("startAnimation"));
      };
      // Allow for all computation to be completed
      setTimeout(() => {
        startAnimation();
        setInterval(startAnimation, animation.totalDuration);
      }, 100);
    },
  });
};
