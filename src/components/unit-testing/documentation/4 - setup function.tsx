import { shallow } from "enzyme";
import * as React from "react";
import * as renderer from "react-test-renderer";
import HelloWorld from "../../../ui/hello-world/HelloWorld";

const setup = (propOverrides?: any) => {
  const defaultProps = {
    message: "test-string"
  };
  const finalProps = Object.assign(defaultProps, propOverrides);
  return {
    props: finalProps,
    wrapper: shallow(<HelloWorld {...finalProps} />)
  };
};

describe("Given the HelloWorld component", () => {
  it("then the message is rendered", () => {
    const { wrapper, props } = setup();
    const message = wrapper.find(`[data-testing-id="hello-world-message"]`);
    expect(message.props().children).toBe(props.message);
  });

  describe("when there are children", () => {
    it("then the children are rendered", () => {
      const testChild = <p>I am the child</p>;
      const { wrapper } = setup({ children: testChild });
      const childrenWrapper = wrapper.find(
        `[data-testing-id="hello-world-children"]`
      );
      expect(childrenWrapper.exists()).toBe(true);
      expect(childrenWrapper.contains(testChild)).toBe(true);
    });

    it("then it matches the snapshot", () => {
      const snapshot = renderer
        .create(
          <HelloWorld message={"test-string"}>
            <p>I am the child</p>
          </HelloWorld>
        )
        .toJSON();
      expect(snapshot).toMatchSnapshot();
    });
  });

  describe("when there are no children", () => {
    it("then the children are not rendered", () => {
      const { wrapper } = setup();
      const childrenWrapper = wrapper.find(
        `[data-testing-id="hello-world-children"]`
      );
      expect(childrenWrapper.exists()).toBe(false);
    });

    it("then it matches the snapshot", () => {
      const snapshot = renderer
        .create(<HelloWorld message={"test-string"} />)
        .toJSON();
      expect(snapshot).toMatchSnapshot();
    });
  });
});
