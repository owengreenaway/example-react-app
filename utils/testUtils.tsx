import { shallow, ShallowWrapper } from "enzyme";
import * as React from "react";
import * as renderer from "react-test-renderer";

export const createSetup = function<T>(
  Component: React.SFC<T>,
  defaultProps: Partial<T>
) {
  interface SetupInterface {
    props: Partial<T>;
    wrapper: ShallowWrapper<T>;
  }

  return (propOverrides?: Partial<T>): SetupInterface => {
    const finalProps: Partial<T> = Object.assign(defaultProps, propOverrides);
    return {
      props: finalProps,
      wrapper: shallow(<Component {...finalProps} />)
    };
  };
};

export const createSnapshot = function<T>(
  Component: React.SFC<T>,
  defaultProps: Partial<T>
) {
  return (props?: Partial<T>): renderer.ReactTestRendererJSON => {
    const finalProps: Partial<T> = Object.assign(defaultProps, props);
    return renderer.create(<Component {...finalProps} />).toJSON();
  };
};

export type ShallowWrapperFunction = (
  enzymeWrapper: ShallowWrapper<any, any>
) => ShallowWrapper<any>;

export const findById = (
  testingId: string
): ShallowWrapperFunction => enzymeWrapper =>
  enzymeWrapper.find(`[data-testing-id="${testingId}"]`);
