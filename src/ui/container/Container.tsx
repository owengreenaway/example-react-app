import * as React from "react";
import styled from "react-emotion";

const Wrapper = styled("section")`
  margin: auto;
  max-width: 50rem;
  padding: 0 1rem;
`;

const Container: React.SFC<{}> = ({ children }) => {
  return <Wrapper>{children}</Wrapper>;
};

export default Container;
