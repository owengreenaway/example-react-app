import { shallow } from "enzyme";
import * as React from "react";
import * as renderer from "react-test-renderer";
import Container from "./Container";

const testChild = <p>I am the child</p>;

const setup = (props?: any) => {
  const defaultPropsOfSetup = {
    children: testChild
  };
  props = Object.assign(defaultPropsOfSetup, props);
  return {
    snapshot: renderer.create(<Container children={props.children} />).toJSON(),
    wrapper: shallow(<Container children={props.children} />)
  };
};

describe("Given the Container component", () => {
  describe("when given children", () => {
    it("then it renders the children", () => {
      const { wrapper } = setup();
      expect(wrapper.contains(testChild)).toBeTruthy();
    });
    it("then it matches the snapshot", () => {
      const { snapshot } = setup();
      expect(snapshot).toMatchSnapshot();
    });
  });
});
