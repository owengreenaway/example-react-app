import * as React from "react";
import styled, { css } from "react-emotion";

const BorderFix = styled("section")`
  margin-bottom: -1px;
  position: relative;
`;

const DefaultIndent = styled("section")`
  padding-left: 40px;
  background: #ddd;
`;

class BaseTree extends React.Component<any, {}> {
  render(): JSX.Element {
    const { dependencies, row, Indent = DefaultIndent } = this.props;
    return (
      <>
        <BorderFix>{row(this.props)}</BorderFix>
        <Indent>
          {dependencies.map((child: any) => (
            <BaseTree {...child} row={row} Indent={Indent} />
          ))}
        </Indent>
      </>
    );
  }
}

// tslint:disable-next-line:max-classes-per-file
class BaseTreePerf extends React.Component<any, {}> {
  render(): JSX.Element {
    const { dependencies, row, Indent = DefaultIndent } = this.props;
    return (
      <>
        {row(this.props)}
        <Indent>
          {dependencies.map((child: any) => (
            <BaseTreePerf {...child} row={row} Indent={Indent} />
          ))}
        </Indent>
      </>
    );
  }
}

const StyledIndent = styled("section")`
  padding-left: 30px;
  background: grey;
`;

const StyledRow = styled("section")`
  border: 1px solid black;
  background: white;
  padding: 5px 15px;
`;

const GoodStyledRow = styled("section")`
  border: 1px solid green;
  background: lightgreen;
  padding: 5px 15px;
`;

const BadStyledRow = styled("section")`
  border: 1px solid red;
  background: lightpink;
  padding: 5px 15px;
`;

const myStyle = css`
  .owen {
    border: 1px solid black;
    background: white;
    padding: 5px 15px;
  }
`;

// tslint:disable-next-line:max-classes-per-file
class LazyLoad extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      loading: true,
      partialArray: this.props.bigArray.slice(0, 100)
    };
  }
  showNextThousand = () => {
    const currentPartialArrayLength = this.state.partialArray.length;
    const newPartialArrayLength = currentPartialArrayLength + 1000;
    this.setState({
      partialArray: this.props.bigArray.slice(0, newPartialArrayLength)
    });
  };

  showAll = () => {
    const currentPartialArrayLength = this.state.partialArray.length;
    const newPartialArrayLength = currentPartialArrayLength + 1000;
    this.setState({
      partialArray: this.props.bigArray.slice(0, newPartialArrayLength)
    });

    if (newPartialArrayLength < this.props.bigArray.length) {
      setTimeout(this.showAll, 1);
    } else {
      this.setState({ loading: false });
    }
  };

  componentDidMount() {
    this.showAll();
  }
  render(): JSX.Element {
    const { partialArray, loading } = this.state;
    console.log("partialArray", partialArray.length);
    return (
      <>
        {loading ? <p>Loading</p> : null}
        {partialArray.map((item: any) => (
          <div
            key={item.key}
            style={{
              paddingLeft: item.formattedDepth
            }}
          >
            <div className="owen">{item.name}</div>
          </div>
        ))}
        <button onClick={this.showNextThousand}>Show next thousand</button>
      </>
    );
  }
}

interface DataInterface {
  name: string;
  dependencies: DataInterface[];
  key: string;
  type?: string;
}

const Example: React.SFC = () => {
  const maxDepth = 4;

  const addChild = (depth: number): DataInterface[] => {
    if (depth > maxDepth) {
      return [];
    }

    const dependencies = [];

    for (let i = 0; i <= maxDepth; i++) {
      dependencies.push({
        dependencies: addChild(depth + 1),
        key: "key " + depth + "-" + i,
        name: "test " + depth + "-" + i
      });
    }

    return dependencies;
  };

  const bigNestedObject: DataInterface = {
    dependencies: addChild(0),
    key: "root",
    name: "root"
  };

  console.log("bigNestedObject", bigNestedObject);

  const data: DataInterface = {
    dependencies: [
      {
        dependencies: [],
        key: "1, 2a",
        name: "1, 2a"
      },
      {
        dependencies: [
          {
            dependencies: [],
            key: "1, 2b, 3",
            name: "1, 2b, 3",
            type: "good"
          }
        ],
        key: "1, 2b",
        name: "1, 2b"
      },
      {
        dependencies: [],
        key: "1, 2c",
        name: "1, 2c",
        type: "bad"
      }
    ],
    key: "1",
    name: "1"
  };

  const nestedObjectToArray = (nestedObject: DataInterface) => {
    const returnArray: any = [];

    const addRow = (row2: DataInterface, depth: number) => {
      returnArray.push({
        depth,
        formattedDepth: `${depth * 30}px`,
        key: returnArray.length,
        name: row2.name,
        type: row2.type
      });
      row2.dependencies.forEach(childRow => {
        addRow(childRow, depth + 1);
      });
    };

    addRow(nestedObject, 0);
    return returnArray;
  };

  const bigArray = nestedObjectToArray(bigNestedObject);

  console.log(bigArray.length);

  const row = (props: any) => {
    if (props.type === "good") {
      return <GoodStyledRow>Selected: {props.name}</GoodStyledRow>;
    }
    if (props.type === "bad") {
      return <BadStyledRow>Selected: {props.name}</BadStyledRow>;
    }
    return <StyledRow>{props.name}</StyledRow>;
  };

  const rowPerf = (props: any) => {
    return <p>{props.name}</p>;
  };

  const indentPerf = (props: any) => {
    return <section>{props.children}</section>;
  };

  return (
    <section className={myStyle}>
      {/* <DefaultIndent>
        <DefaultIndent>
          <p>hi</p>
        </DefaultIndent>
      </DefaultIndent> */}

      {/* <BaseTree
        name={data.name}
        dependencies={data.dependencies}
        row={row}
        Indent={StyledIndent}
      /> */}

      {/* <BaseTree
        name={bigNestedObject.name}
        dependencies={bigNestedObject.dependencies}
        row={row}
      /> */}

      {/* <BaseTreePerf
        name={bigNestedObject.name}
        dependencies={bigNestedObject.dependencies}
        row={rowPerf}
        Indent={indentPerf}
      /> */}

      {/* <BaseTreePerf
        name={data.name}
        dependencies={data.dependencies}
        row={rowPerf}
        Indent={indentPerf}
      /> */}

      {/* {bigArray.map((item: any) => (
        <div
          key={item.key}
          style={{
            paddingLeft: item.formattedDepth
          }}
        >
          <div className="owen">{item.name}</div>
        </div>
      ))} */}

      <LazyLoad bigArray={bigArray} />
    </section>
  );
};

export default Example;

//
// open / close
// row
