import * as React from "react";

export class LazyLoad extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      loading: true,
      partialArray: this.props.bigArray.slice(0, 100)
    };
  }

  showNextThousand = () => {
    const currentPartialArrayLength = this.state.partialArray.length;
    const newPartialArrayLength = currentPartialArrayLength + 1000;
    this.setState({
      partialArray: this.props.bigArray.slice(0, newPartialArrayLength)
    });
  };

  showAll = () => {
    const currentPartialArrayLength = this.state.partialArray.length;
    const newPartialArrayLength = currentPartialArrayLength + 5000;
    this.setState({
      partialArray: this.props.bigArray.slice(0, newPartialArrayLength)
    });

    if (newPartialArrayLength < this.props.bigArray.length) {
      setTimeout(this.showAll, 1);
    } else {
      this.setState({ loading: false });
      const now = new Date();
      const timeInMilliSeconds = now.getTime() - this.props.timestamp.getTime();
      console.log(
        "LazyLoading completely rendered after",
        timeInMilliSeconds,
        "ms"
      );
    }
  };

  componentDidMount() {
    setTimeout(this.showAll, 1);
  }

  render(): JSX.Element {
    const { partialArray, loading } = this.state;
    return (
      <>
        {loading ? (
          <div
            style={{
              backgroundColor: "#ff5e5e",
              color: "#842727",
              display: "block",
              left: 0,
              padding: "15px",
              position: "fixed",
              textAlign: "center",
              top: "0",
              width: "100%"
            }}
          >
            Loading {partialArray.length} of {this.props.bigArray.length}
          </div>
        ) : null}
        {partialArray.map((item: any) => (
          <div
            key={item.key}
            style={{
              background: "grey",
              paddingLeft: item.formattedDepth
            }}
          >
            <div className="unStyledRow">{item.name}</div>
          </div>
        ))}
      </>
    );
  }
}
