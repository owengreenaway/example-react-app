import * as React from "react";
import styled from "react-emotion";

const Wrapper = styled("h2")`
  font-size: 1.5rem;
  text-align: right;
  margin: 3rem 0;
  line-height: 1em;
  font-weight: 200;
  @media (min-width: 768px) {
    font-size: 2rem;
  }
`;

export interface Props {
  copy: string;
}

export const Title: React.SFC<Props> = ({ copy }) => {
  return <Wrapper>{copy}</Wrapper>;
};
