import * as React from "react";

// Scroll restoration based on https://reacttraining.com/react-router/web/guides/scroll-restoration.
class ScrollToTopOnMount extends React.Component<any, any> {
  componentDidMount(): void {
    window.scrollTo(0, 0);
  }
  render(): JSX.Element {
    return null;
  }
}

export default ScrollToTopOnMount;
