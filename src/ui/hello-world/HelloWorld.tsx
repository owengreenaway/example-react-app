import * as React from "react";
import styled from "react-emotion";

const Wrapper = styled("section")`
  padding: 15px;
`;

const Text = styled("p")`
  color: red;
`;

const Children = styled("div")`
  background: grey;
`;

export interface HelloWorldProps {
  message: string;
  children?: React.ReactNode;
}

const HelloWorld: React.SFC<HelloWorldProps> = ({ message, children }) => {
  return (
    <Wrapper>
      <Text>Static text</Text>
      <Text data-testing-id="hello-world-message">{message}</Text>
      {children ? (
        <Children data-testing-id="hello-world-children">{children}</Children>
      ) : null}
    </Wrapper>
  );
};

export default HelloWorld;
