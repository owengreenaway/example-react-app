import * as React from "react";
import { Link } from "react-router-dom";
import ScrollToTopOnMount from "../../app/ScrollToTopOnMount";
import Container from "../../ui/container/Container";

const Blog = ({}) => {
  return (
<Container>
  <ScrollToTopOnMount />
    <header className="header">
      <Link to="/"><h2>Home</h2></Link>
    </header>

    <article>
      <div className="timestamp">26/11/2017 &middot;</div>
      <h2>Stopped adding to this micro-blog</h2>
    </article>

    <article>
      <div className="timestamp">25/11/2017 &middot;</div>
      <h2>React 16.2.0</h2>
      <div>
        <p>A new version of React is out with new <a href="https://reactjs.org/blog/2017/11/28/react-v16.2.0-fragment-support.html">fragment support</a>.</p>
      </div>
    </article>

    <article>
      <div className="timestamp">18/11/2017 &middot;</div>
      <h2>Ben McCormick</h2>
      <div>
        <p>I've really enjoyed reading <a href="https://benmccormick.org/2017/07/19/ten-things-javascript/">Ben's blog</a>.</p>
      </div>
    </article>

    <article>
      <div className="timestamp">11/11/2017 &middot;</div>
      <h2>React component choices</h2>
      <div>
        <p>Consensus seems to be forming for most of these  <a href="https://medium.freecodecamp.org/8-key-react-component-decisions-cc965db11594">choices</a> but for a lot of things it's just too early to tell what is best practise.</p>
      </div>
    </article>

    <article>
      <div className="timestamp">04/11/2017 &middot;</div>
      <h2>Three letter acronymns (TLA)</h2>
      <div>
        <p>I'd forgotten what DOM stood for so I spent a few minutes <a href="http://www.hongkiat.com/blog/web-dev-acronyms/">recapping the acronymns.</a></p>
      </div>
    </article>

    <article>
      <div className="timestamp">28/10/2017 &middot;</div>
      <h2>CSSOM</h2>
      <div>
        <p>I knew about the DOM but I didn't know about <a href="http://www.hongkiat.com/blog/css-object-model-cssom/">CSSOM</a> before reading this article.</p>
      </div>
    </article>

    <article>
      <div className="timestamp">21/10/2017 &middot;</div>
      <h2>Higher order components</h2>
      <div>
        <p>I don't mind admitting I found <a href="https://btnwtn.com/articles/higher-order-components-for-beginners">higher order components</a> quite difficult to understand at first but I am making good progress with them now.</p>
      </div>
    </article>

    <article>
      <div className="timestamp">22/07/2017 &middot;</div>
      <h2>Code snippets</h2>
      <div>
        <p>I love these <a href="https://github.com/xabikos/vscode-react">code snippets</a> for creating React components.
          After a small time memorising the shortcuts they have sped up my workflow and made it more enjoyable.
          It's also had the benefit that creating a new component is quick and easy and so I'm more likely to do the right thing and break up large components
          when needed instead of thinking "urgh, that's too much effort".</p>
      </div>
    </article>

    <article>
      <div className="timestamp">15/07/2017 &middot;</div>
      <h2>Career progression</h2>
      <div>
        <p>I found this article on <a href="https://hackernoon.com/why-finding-10x-developers-is-so-hard-8d1d57ec6f0c">developer career progression</a> very interesting. I agree with the
        coder, programmer, developer, architect, innovator progression and it did make me think about my own career progression.</p>
      </div>
    </article>

    <article>
      <div className="timestamp">08/07/2017 &middot;</div>
      <h2>React Styleguidist</h2>
      <div>
        <p>I found a really cool React documentation generator called <a href="https://react-styleguidist.js.org/">React Styleguidist</a>. I definitely think that
        the code comments need to be in the code files instead of a separate wiki. Separate files will easily go out of date whereas if the documentation is straight in the code
        then it will changed in the same pull request. I've set this up and will use it as our living style guide. It can also build a static version so I might ask my team to add to that to
        the continuous deployment so we can share links to components by hosting it on the dev server.</p>
      </div>
    </article>

    <article>
      <div className="timestamp">01/07/2017 &middot;</div>
      <h2>Detecting unused CSS</h2>
      <div>
        <p>I use this useful tool called <a href="https://github.com/purifycss/purifycss">purifycss</a> to detect the possibly unused css in a legacy project.
        It's very easy for the css in a project to become "only add" as no-one knows how it all works exactly and so are too scared to delete anything.
        I think BEM styling can really help with this issue if everything is namespaced. SCSS then helps makes the namespacing less repetitive.
        My biggest aims when setting up a new project is to carefully architect how I am going to keep the css base as deletable as possible.</p>
      </div>
    </article>

    <article>
      <div className="timestamp">25/06/2017 &middot;</div>
      <h2>Expectation</h2>
      <div>
        <p>An interesting idea for improving culture by thinking about the phrase <a href="https://hackernoon.com/just-do-as-expected-544385144f14">"just do as expected"</a>.
        I think the company culture is vital for happy, productive programmers. Knowing what is required or expected of you can be very stressful if not
        communicated clearly.</p>
      </div>
    </article>

    <article>
      <div className="timestamp">17/06/2017 &middot;</div>
      <h2>React Animation</h2>
      <div>
        <p>I loved this <a href="https://www.youtube.com/watch?v=W5AdUcJDHo0">video on React animations</a>.
        I then read this article to introduce me to <a href="https://medium.com/@nashvail/a-gentle-introduction-to-react-motion-dc50dd9f2459">react motion.</a>
        React collapse is a really nice wrapper to make the simple animation much easier to work with. I'm still looking for a tool to create animations
        without code. I think I need to try inVision. Once I've created some animations I'll try to recreate them with React Motion.</p>
      </div>
    </article>

    <article>
      <div className="timestamp">10/06/2017 &middot;</div>
      <h2>Blogging</h2>
      <div>
        <p>I've been bookmarking sites I wanted to blog about over the last few months but have lost track of all the dates so am just going to publish all the blogs with today's date.</p>
      </div>
    </article>

    <article>
      <div className="timestamp">10/06/2017 &middot;</div>
      <h2>Smashing Magazine: Designing forms</h2>
      <div>
        <p>I was pleased that this <a href="https://www.smashingmagazine.com/2017/06/designing-efficient-web-forms">article on efficient form design</a> agrees with my UX ideas on performant forms. I think forms are especially important and their functionality has to trump their form.</p>
      </div>
    </article>

    <article>
      <div className="timestamp">10/06/2017 &middot;</div>
      <h2>Yarn</h2>
      <div>
        <p>Yarn is the improvement I didn't realise I needed until I used it! I needed to upgrade my npm anyway and so installed yarn as well basically to see what all the fuss was about. It appears much faster, better for repeat installs, better feedback as it installs and I love the lockfile idea. Unfortunately the Karma test runner I use has a bug so doesn't work with yarn but once that's fixed I'll probably swap over.</p>
      </div>
    </article>

    <article>
      <div className="timestamp">10/06/2017 &middot;</div>
      <h2>Improving at unit testing</h2>
      <div>
        <p>I have been practising unit testing which has involved a lot of reading the <a href="http://airbnb.io/enzyme/docs/api/ReactWrapper/forEach.html">Enzyme docs</a> and again <a href="https://facebook.github.io/react/docs/typechecking-with-proptypes.html">RTFM</a>.</p>
      </div>
    </article>

    <article>
      <div className="timestamp">10/06/2017 &middot;</div>
      <h2>Integration testing</h2>
      <div>
        <p>I read this useful <a href="https://hackernoon.com/testing-your-frontend-code-part-iv-integration-testing-f1f4609dc4d9#.b8jt5ffpp"> blog on integration testing.</a></p>
      </div>
    </article>

    <article>
      <div className="timestamp">10/06/2017 &middot;</div>
      <h2>Setting up unit testing</h2>
      <div>
        <p>Once I knew what testing platform I wanted I had to <a href="http://attackofzach.com/setting-up-a-project-using-karma-with-mocha-and-chai/">read this blog on how to set it up!</a></p>
      </div>
    </article>

    <article>
      <div className="timestamp">10/06/2017 &middot;</div>
      <h2>Picking a unit testing platform</h2>
      <div>
        <p>I knew I wanted to use Karma as the test runner and <a href="http://airbnb.io/enzyme/docs/api/shallow.html">Enzyme</a> seems the most popular React interface. I then had to chose between Jasmine, Jest and Mocha. I've gone for Mocha with its addons Chai and Sinon.</p>
      </div>
    </article>

    <article>
      <div className="timestamp">10/06/2017 &middot;</div>
      <h2>Form Validation</h2>
      <div>
        <p>There are multiple ways to validate forms in React and it's taken me a while to think about all the pros and cons. I really like controlled components but generalised helper functions can reduce a lot of the boilerplate.</p>
        <p>I've been reading lots of blogs <a href="https://hackernoon.com/how-to-do-simple-form-validation-in-reactjs-83b92c080b67#.b89g6ky3i">like this one on validation.</a></p>
      </div>
    </article>

    <article>
      <div className="timestamp">10/06/2017 &middot;</div>
      <h2>ES6</h2>
      <div>
        <p>As a JavaScript developer I constantly need to learn the latest features in <a href="https://medium.com/sons-of-javascript/javascript-an-introduction-to-es6-1819d0d89a0f#.thr2rytad ">ES6</a>. My favourite feature being <a href="https://leanpub.com/understandinges6/read#leanpub-auto-multiline-strings">the new string manipulation</a> features.</p>
      </div>
    </article>

    <article>
      <div className="timestamp">10/06/2017 &middot;</div>
      <h2>Improving page speed</h2>
      <div>
        <p>At work we are currently still building the site and adding features but I'm looking forward to <a href="https://hackernoon.com/improving-first-time-load-of-a-production-react-app-part-1-of-2-e7494a7c7ab0#.pwd3ofbzq">optimising the page speed</a>.</p>
      </div>
    </article>

    <article>
      <div className="timestamp">10/06/2017 &middot;</div>
      <h2>How to write good comments</h2>
      <div>
        <p>I would really like a <a href="https://hackernoon.com/never-forget-anything-before-after-and-while-coding-98d187ae4cf1#.qvcs6ecoj">styleguide for my team to use to write comments</a>. Unfortunately this is quite far down the todo list at the moment.</p>
      </div>
    </article>

    <article>
      <div className="timestamp">10/06/2017 &middot;</div>
      <h2>Augmented Reality</h2>
      <div>
        <p>My prediction is that <a href="https://hackernoon.com/follow-the-yellow-brick-road-da5112bcdb74#.f7xpcq8tk">Augmented Reality</a> is the biggest technology boom in 2020-2030.</p>
      </div>
    </article>

    <article>
      <div className="timestamp">10/06/2017 &middot;</div>
      <h2>React and CSS</h2>
      <div>
        <p>Again there are too many choices. Unfortunately I don't have time to try them all thoroughly.
          <a href="https://glenmaddern.com/articles/css-modules">Css modules</a> enforce BEM but also add another level of abstraction and complexity
          for people just used to basic css. <a href="https://medium.com/@Connorelsea/using-sass-with-create-react-app-7125d6913760#.8xls6vbvo">SCSS</a>
          is easily for people who only know CSS. I'm currently in a startup and I don't know how quickly we'll employ new people, nor their experience,
          so keeping the barrier to entry low is quite important. Everyone in the company already knows SCSS so there is no training time required.
        </p>
      </div>
    </article>

    <article>
      <div className="timestamp">10/06/2017 &middot;</div>
      <h2>Using new React libraries</h2>
      <div>
        <p>We needed number formatting for credit cards details and phone numbers. We also need telephone validation including mandatory country codes.
          It's always hard to know if I should build a component and logic myself, use someone else's component or build the component but use a js library
          for the logic. I ended up using
          <a href="https://github.com/halt-hammerzeit/react-phone-number-input">React phone number</a> but I'm still not sure if it's exactly correct.
          Another useful looking library was
          <a href="https://github.com/nosir/cleave.js">cleave.js</a>.</p>
      </div>
    </article>

    <article>
      <div className="timestamp">10/06/2017 &middot;</div>
      <h2>React Accessibility</h2>
      <div>
        <p><a href="https://github.com/romeovs/react-a11y">React a11y</a> enforces good accessibility practices and teaching people about
        accessibility. I think it's fantastic and easy to set up with Eslint.</p>
      </div>
    </article>

    <article>
      <div className="timestamp">10/06/2017 &middot;</div>
      <h2>Create React App</h2>
      <div>
        <p>I really need to set aside some time to add <a href="http://chrisshepherd.me/posts/adding-hot-module-reloading-to-create-react-app">hot reloading</a></p>
      </div>
    </article>
    <article id="p46">
      <div className="timestamp">25/02/2017 &middot;</div>
      <h2>Buttons and Colours</h2>
      <div>
        <p>I've been looking at these <a href='https://bttn.surge.sh/'>examples of buttons</a> and <a href='http://colorsupplyyy.com'>this resource on colour combinations.</a>
        </p>
      </div>
    </article>
    <article id="p45">
      <div className="timestamp">18/02/2017 &middot;</div>
      <h2>Writing clean JavaScript</h2>
      <div>
        <p>A useful resource with good examples on how to write <a href='https://github.com/ryanmcdermott/clean-code-javascript'>clean JavaScript.</a> Definitely something to keep referring back to.</p>
      </div>
    </article>
    <article id="p44">
      <div className="timestamp">11/02/2017 &middot;</div>
      <h2>JavaScript debugging</h2>
      <div>
        <p>Recently I've been trying to use <a href='https://developers.google.com/web/tools/chrome-devtools/javascript/step-code'>Chrome devtools</a> more effective. Learning how to step through my code has helped me quickly find the source of bugs.</p>
      </div>
    </article>
    <article id="p43">
      <div className="timestamp">04/02/2017 &middot;</div>
      <h2>Git is incredible complicated</h2>
      <div>
        <p>Git is <a href='https://hackernoon.com/git-in-2016-fad96ae22a15#.o41gnfpyl'>more complicated</a> that I ever imagined!</p>
      </div>
    </article>
    <article id="p42">
      <div className="timestamp">28/01/2017 &middot;</div>
      <h2>Umbraco CMS</h2>
      <div>
        <p>I have previously learnt how to make custom templates using <a href='www.squarespace.com'>squarespace</a> but found it quite limiting. My colleagues use Umbraco and so I am trying to learn how to use it. The first thing I have had to learn is the difference <a href='http://24days.in/umbraco-cms/2015/strongly-typed-vs-dynamic-content-access/'>between strongly typed and dynamic content</a>.</p>
      </div>
    </article>
    <article id="p41">
      <div className="timestamp">21/01/2017 &middot;</div>
      <h2>Penetration Testers Guide</h2>
      <div>
        <p>This article made me realise that <a href='https://hackernoon.com/the-2017-pentester-guide-to-windows-10-privacy-security-cf734c510b8d#.24zvt5eo3'>I don't actually know much about security</a>.</p>
      </div>
    </article>
    <article id="p40">
      <div className="timestamp">14/01/2017 &middot;</div>
      <h2>Virtual Reality Interfaces</h2>
      <div>
        <p>Lots of interesting articles by <a href='http://smus.com/ray-input-webvr-interaction-patterns/'>Boris Smus</a>.</p>
      </div>
    </article>
    <article id="p39">
      <div className="timestamp">07/01/2017 &middot;</div>
      <h2>React Interview Questions</h2>
      <div>
        <p>I learnt about some <a href='https://tylermcginnis.com/react-interview-questions/'>React interview questions</a>.</p>
      </div>
    </article>
    <article id="p38">
      <div className="timestamp">31/12/2016 &middot;</div>
      <h2>I discovered Hackernoon</h2>
      <div>
        <p>Over the Christmas and New Year holidays I've been reading all the articles on <a href='https://hackernoon.com/'>hackernoon.com</a>
        </p>
      </div>
    </article>
    <article id="p37">
      <div className="timestamp">24/12/2016 &middot;</div>
      <h2>Learning design</h2>
      <div>
        <p>I've been practising my designing and <a href='https://www.smashingmagazine.com/2016/12/mistakes-developers-make-when-learning-design/'>this smashing magazine article was useful</a>
        </p>
      </div>
    </article>
    <article id="p36">
      <div className="timestamp">17/12/2016 &middot;</div>
      <h2>Shorthand or Longhand CSS</h2>
      <div>
        <p>A very interesting article on CSS-tricks about <a href='http://csswizardry.com/2016/12/css-shorthand-syntax-considered-an-anti-pattern/'>using longhand and shorthand</a>
        </p>
      </div>
    </article>
    <article id="p35b">
      <div className="timestamp">10/12/2017 &middot;</div>
      <h2>Greater.jobs</h2>
      <div>
        <p>Phew, the last few weeks have been really busy being the lead front end developer on <a href='//www.greater.jobs'>greater.jobs</a>. I'm very happy with how it turned out.</p>
      </div>
    </article>
    <article id="p35">
      <div className="timestamp">03/09/2016 &middot;</div>
      <h2>Battling BEM</h2>
      <div>
        <p>This is a great article on how to <a href='https://www.smashingmagazine.com/2016/06/battling-bem-extended-edition-common-problems-and-how-to-avoid-them/'>solve some of problems you face when using BEM</a>.</p>
      </div>
    </article>
    <article id="p34">
      <div className="timestamp">27/08/2016 &middot;</div>
      <h2>Using Let in JavaScript</h2>
      <div>
        <p>This week I found out about using "let" instead of "var" in JavaScript, especially in for loops. I must admit I haven't actually used it yet and I need to research a bit more about how the two differ.</p>
      </div>
    </article>
    <article id="p33">
      <div className="timestamp">20/08/2016 &middot;</div>
      <h2>IE8 conditional stylesheet</h2>
      <div>
        <p>I knew about conditional HTML but had never used it. We used it to add a conditional CSS file with IE8 fixes.</p>
      </div>
    </article>
    <article id="p32">
      <div className="timestamp">13/08/2016 &middot;</div>
      <h2>VR Control method examples</h2>
      <div>
        <p>I added some new control methods to the <a href='vr.html'>virtual reality examples</a> page.</p>
      </div>
    </article>
    <article id="p31">
      <div className="timestamp">06/08/2016 &middot;</div>
      <h2>Updated the styleguide</h2>
      <div>
        <p>I updated the <a href='styleguide.html'>styleguide</a>.</p>
      </div>
    </article>
    <article id="p30">
      <div className="timestamp">30/07/2016 &middot;</div>
      <h2>My first 100% backend amend</h2>
      <div>
        <p>A client wanted a section removed from their website. The backend team were busy so on my local version of the site I found the relevant code and editted the razor file. The backend guys gave my work a quick check and I pushed my change to staging. My first 100% backend client amend.</p>
      </div>
    </article>
    <article id="p29">
      <div className="timestamp">23/07/2016 &middot;</div>
      <h2>Using SquareSpace's CMS</h2>
      <div>
        <p>I cloned my sister's website and linked it up to the SquareSpace CMS. Her website is 100% custom so their CMS didn't have all the features I wanted but it was a good learning experience.</p>
      </div>
    </article>
    <article id="p28">
      <div className="timestamp">16/07/2016 &middot;</div>
      <h2>Virtual Reality using Kolor.com</h2>
      <div>
        <p>I created a Virtual Reality experience without code using <a href='http://www.Kolor.com'>Kolor.com</a>. They have basically built exactly what I was thinking of creating if I had the time. I really like how they use small videos on top of a photosphere to give the impression of a videosphere.</p>
      </div>
    </article>
    <article id="p27">
      <div className="timestamp">09/07/2016 &middot;</div>
      <h2>CSS breakpoints</h2>
      <div>
        <p>I had a good discussion with a freelancer over if we should use only min-width or use both min and max widths? I can see advantages for both. Currently sticking with only using min-width.</p>
      </div>
    </article>
    <article id="p26">
      <div className="timestamp">02/07/2016 &middot;</div>
      <h2>Creating jQuery plugins</h2>
      <div>
        <p>The design patterns book inspired me to learn more about <a href='https://learn.jquery.com/'>jQuery plugins</a>. I used this professionally to isolate a specific jQuery version for the social wall.</p>
      </div>
    </article>
    <article id="p25">
      <div className="timestamp">25/06/2016 &middot;</div>
      <h2>More JavaScript Patterns</h2>
      <div>
        <p>I then read about <a href='https://addyosmani.com/resources/essentialjsdesignpatterns/book/'>JavaScript design patterns in far more detail.</a>
        </p>
      </div>
    </article>
    <article id="p24">
      <div className="timestamp">18/06/2016 &middot;</div>
      <h2>JavaScript Modules</h2>
      <div>
        <p>I learnt about the basic <a href='https://scotch.io/bar-talk/4-javascript-design-patterns-you-should-know'>JavaScript design patterns.</a>
        </p>
      </div>
    </article>
    <article id="p23">
      <div className="timestamp">11/06/2016 &middot;</div>
      <h2>Optimising my jQuery</h2>
      <div>
        <p>I read more about <a href='https://learn.jquery.com/'>improving and optimising my jQuery.</a>
        </p>
      </div>
    </article>
    <article id="p22">
      <div className="timestamp">04/06/2016 &middot;</div>
      <h2>Git Archive</h2>
      <div>
        <p>New Git command of the week:</p><pre><code>git archive --format zip --output /full/path/to/zipfile.zip master</code></pre>
        <p>Used to package up our files to send to the client, with the git repository removed.</p>
      </div>
    </article>
    <article id="p21">
      <div className="timestamp">28/05/2016 &middot;</div>
      <h2>Components</h2>
      <div>
        <p>This <a href='http://www.owlcarousel.owlgraphic.com/' target='_blank'>carousel plugin</a> looks good and I might use it in my next project.</p>
        <p> <a href='http://noelboss.github.io/featherlight/' target='_blank'>Featherlight</a> minimal popups has been very good and this <a href='https://github.com/aFarkas/lazysizes' target='_blank'>lazy loading plugin</a> is great for image heavy designs.</p>
      </div>
    </article>
    <article id="p20">
      <div className="timestamp">21/05/2016 &middot;</div>
      <h2>WebSpeech API</h2>
      <div>
        <p>The <a href='https://iceddev.com/blog/jarvis-an-amazon-echo-clone-in-your-browser/' target='_blank'>webspeech API</a> looks interesting but at the moment feels a bit of a gimmick. I might look into local speech recognition and use it for my home automation.</p>
      </div>
    </article>
    <article id="p19">
      <div className="timestamp">14/05/2016 &middot;</div>
      <h2>A-Frame.io Virtual Reality</h2>
      <div>
        <p>Recently I've thrown myself into learning <a href="https://aframe.io/" target="_blank">A-Frame</a> and you can see the results <a href="vr.html" target="_blank">on my vr page.</a>
        </p>
      </div>
    </article>
    <article id="p18">
      <div className="timestamp">07/05/2016 &middot;</div>
      <h2>$0 in Chrome Dev Tools</h2>
      <div>
        <p>Chrome dev tools updated this week and a mystery '$0' appeared. I quick search <a href='https://willd.me/posts/0-in-chrome-dev-tools' target='_blank'>revealed the answer</a>. It's actually quite handy.</p>
      </div>
    </article>
    <article id="p17">
      <div className="timestamp">30/04/2016 &middot;</div>
      <h2>Website Obesity</h2>
      <div>
        <p>Website obesity was mentioned again as <a href='https://mobiforge.com/research-analysis/the-web-is-doom'>this article</a> did the rounds on social media.</p>
        <p>I'm glad the topic is mentioned but the article uses a lightly odd version of 'average weight' and also apparently refers to the zipped shareware Doom game that is far smaller than the full game.</p>
        <p>Website obesity is a battle I am currently having professionally. I am using a two-pronged approach. Firstly I am using my technical skills to cope with the designs. For example I am lazy loading images and optimising them as much as possible. Secondly I am trying to effectively communicate to designers and strategists the size implications of their choices.</p>
        <p>My biggest success so far has been to communicate the idea that instead of just one full screen image to use a CSS background with images only uses for the foreground elements. This approach also helps position the foreground elements so then are never obscured by text.</p>
        <p>I have to be careful that my technological solutions don't undermine my warnings about the implications of page bloat.</p>
      </div>
    </article>
    <article id="p16">
      <div className="timestamp">23/04/2016 &middot;</div>
      <h2>Interview Questions</h2>
      <div>
        <p>This repo has a huge list of <a href='https://github.com/h5bp/Front-end-Developer-Interview-Questions'>front end developer [interview] questions.</a>
        </p>
        <p>I'm not ashamed to admit that I don't know some of the answers. My short-term plan is to know an answer to every question. Over the longer term I can expand my understanding of each topic.</p>
        <p>I'm not planning on being interviewed any time soon but I feel going through this list periodically and learning sections in more depth is always going to be a good idea. Continuous professional development is required and fortunately enjoyable.</p>
      </div>
    </article>
    <article id="p15">
      <div className="timestamp">16/04/2016 &middot;</div>
      <h2>Good Habits</h2>
      <div>
        <p>This week I read a <a href='https://medium.com/@shekhargulati/7-habits-i-wish-every-junior-programmer-should-have-d0d6d8a972c9#.6w6x2n5bb'>useful article</a> by Shekhar Gulati that repeated what I have been doing over the last 9 months.</p>
        <p>I am going to start doing more in the command line, for example create and removing files. I think the <a href='https://www.reddit.com/r/ProgrammerHumor/comments/2mb2cy/command_line_russian_roulette/'>command line Russian Roulette</a> joke, and just 'rm -rf' in general, has made me too apprehensive and I want to fix that.</p>
      </div>
    </article>
    <article id="p14">
      <div className="timestamp">09/04/2016 &middot;</div>
      <h2>Visual Studio Code vs Sublime</h2>
      <div>
        <p>After 9 months I was bored of Sublime and wanted to see what else was available. My co-worker was using Visual Studio Code and I was slightly jealous.</p>
        <p>I've been very impressed with VS Code. Installation was easy, I only needed one plugin to match all of my previous Sublime plugins.</p>
        <p>All but one of my shortcuts work. In Sublime you can hold shift while pasting to match the indention. This is missing from VS Code, which is annoying, but there are other auto indentation methods which I am sure I will get use to.</p>
        <p>It also has brilliant git support which is fantastic.</p>
        <p>Overall I am very impressed and glad I've made the switch.</p>
      </div>
    </article>
    <article id="p13">
      <div className="timestamp">02/04/2016 &middot;</div>
      <h2>React 15</h2>
      <div>
        <p>Over Christmas I learnt React. React on its own is pretty simple but learning ES6, Webpack configuration, Redux and other paradigms, frameworks and conventions has been difficult.</p>
        <p>React <a href='https://facebook.github.io/react/blog/2016/02/19/new-versioning-scheme.html'>switching versions</a> from 0.14 to 15 is weird but also makes sense. Hopefully a client will pay for a project where React is the best choice. Probably via an app using Cordova. At the moment I will just continue learning more about React and its ecosystem.</p>
      </div>
    </article>
    <article id="p12">
      <div className="timestamp">26/03/2016 &middot;</div>
      <h2>You might not need jQuery</h2>
      <div>
        <p>I'm still trying to formulate my views on using libraries instead of vanilla code. I think it's best to scope out the website and see the required functionality. Normally you'll need so much that jQuery is the correct choice (for performance, lack of bugs, compatibility e.t.c) but if you have very little then vanilla is definitely an option.</p>
        <p><a href='http://youmightnotneedjquery.com/'>youmightnotneedjquery.com</a> made me laugh and has useful examples.</p>
        <p><a href='http://youmightnotneedyoumightnotneedjquery.com/'>youmightnotneedyoumightnotneedjquery.com</a> is a bit silly...</p>
      </div>
    </article>
    <article id="p11">
      <div className="timestamp">19/03/2016 &middot;</div>
      <h2>Mocha and Test Driven Development</h2>
      <div>
        <p>I spent an afternoon using <a href="https://mochajs.org/">Mocha</a> and I can now run simple tests. I will try to use the TTD approach the next time I make a service. It's definitely how I would like to code but my current projects aren't really applicable due to their lack of complex JavaScript.</p>
      </div>
    </article>
    <article id="p10">
      <div className="timestamp">12/03/2016 &middot;</div>
      <h2>Susy</h2>
      <div>
        <p>I am now learning <a href="http://susy.oddbird.net/">Susy</a> so I can stop using Bootstrap. I dislike the bloat in my HTML and want to move the presentational elements into my css. Hopefully this will reduce overall size and make my projects clearer.</p>
      </div>
    </article>
    <article id="p9">
      <div className="timestamp">05/03/2016 &middot;</div>
      <h2>jQuery Pros and Cons</h2>
      <div>
        <p>There are many reasons to want to drop jQuery and instead just write in vanilla JavaScript. For a start it's 86k and often you don't need the majority of it. The arguments to keep using it that I have read have been weak and focus on ease of use or its popularity. Then I read a really good comment by <a href="https://toddmotto.com/is-it-time-to-drop-jquery-essentials-to-learning-javascript-from-a-jquery-background/">HonoredMule.</a>
        </p>
        <h3>This is the best pro jQuery argument I have seen:</h3>
        <p>Let's throw away AngularJS, EmberJS, Bootstrap, CSS grids, Modernizr, Underscore.js, Moustache, Backbone, e.t.c. while we're at it.</p>
        <p>If you're talking about learning the basic tools of your trade, then by all means write raw javascript with reckless abandon. It is a core competency and if you can write jQuery (not actually a language) but can't write javascript (the language you are writing when you write jQuery) then you don't even actually know what you're doing and certainly need to address that. Once you master the basic language (or perhaps more to the point, functional programming which is the real learning curve even for experienced OOP developers), move on to language-appropriate design patterns and techniques like functions as generators/compilers, the prototype system, and the Yahoo module pattern.</p>
        <p>And then, when you're ready to do real work - in a timely manner, on a budget, and with well-architected, maintainable results - rely on these well-established 3rd-party libraries without reservation. Don't just use them when you must. Pick an appropriate subset for your problem domain and use them whenever you can, as much as you can.</p>
        <p>These tools, of which jQuery is easily one of the greatest (certainly the most broadly applicable for front-end work), are not merely a compromise against some "purist" perfection or a shortcut to results. They represent and promote solid architectural approaches, widely shared and established domain knowledge, additional design patterns with easy integration points for bringing multi-team work into a cohesive whole. All those developers who call jQuery /the/ solution aren't suffering from a shared delusion - they know something and many of them learned it the hard way, powered by the developer's archetypal self-assurance and NIH syndrome. Besides, every line of 1st-party code you shave (by using libraries and the terse/efficient techniques they enable, not by writing dense logic) reduces the number of mistakes you make, the overall complexity of your project, and the learning curve for other people trying to decipher your code. By extension it increases the long-term maintainability of your code for you and anyone else *even if you didn't really need it /yet/.*</p>
        <p>Case in point: jQuery doesn't just give us fancy selectors, it also encourages and aids valuable design patterns like event-driven programming (with custom events, not just builtin DOM events) and taking advantage of DOM event bubbling, and takes over handler recycling, resulting in far leaner and more loosely coupled code that is better memory managed, avoids callback hell, and minimizes overhead in both code and memory usage. Were all the complex underpinnings for that not wrapped in such an elegant, easy to use pattern, few would even bother maintaining their event and DOM integration. And when they do, they get to choose between building their own NIH-driven solution with minimal support and mindshare and writing scads of boilerplate spaghetti code just begging to introduce memory leaks, bugs, or both. It doesn't take much to put you already well on your way to creating an unmaintainable mass, because you started with a mindset more concerned with minimal dependencies and 3rd-party tool usage than solid, proven architecture.</p>
        <p>Popular software libraries are the exemplar economies of near-infinite scale. Talented developers are only more empowered by their use, and less-talented developers are at least guided toward less destructive paths by the communities and the ways that these tools want to be used. The downsides of using them are often so pitiful we find ourselves quibbling over the inclusion of tens of kilobytes in the cross-domain CDN-cached static resource load with our half-meg-on-every-load pages.</p>
        <p>Now I might step back for a moment and recognize that you're only suggesting one particular library might be losing its lead over the underyling tools it on which it is built. But I doubt you'd even be saying that if not for the sake of a sensationalist headline. jQuery remains the most beloved, broadly applicable, and established of them all, because it provides so many tools that are constantly valuable to everyone. A couple years ago it surpassed Flash as the most used single tool of any sort on the web. And while native javascript today is finally getting some minor, rudimentary advancements that represent a tiny subset of jQuery's decade-old capabilities, the committee-designed javascript of tomorrow will also have to contend with the visionary-led jQuery (or successor thereof) *of tomorrow.*</p>
        <p>Which do you think will advance faster? And more importantly, for what scale of project is an underlying language that (by nature of only being a language) provides few design patterns, no architecture, and no significant domain knowledge truly going to suffice?</p>
        <p>Hooray for HTML5 and things like the Promise API and a decent subset of jQuery's (Sizzle's) selector engine, but these advancements are only adding to our toolset, with no significant impact on our existing tools at all. At the end of the day, "I can do this one particular thing about as well with native code anyway" is a terrible reason to throw away a solid and reliable abstraction layer for all but the most pitifully simplistic of use-cases - namely the case where that one things is *all* you have to do.</p>
        <p>This is not yet a scenario I've encountered in the real world, but if I did I'd still be hard-pressed to give it any special consideration. Instead I'll just keep following the best practices for the tools I use, which covers things like separating native-backed selector resolution from custom jQuery-provided ones to maximize utility without compromizing performance available through native-backed underpinnings, and even on occasion implementing custom selectors or filters to provide a clean abstraction layer around less common use-cases.</p>
        <p>Gaining experience and writing good code are wildly different things, and while the former enables the latter, it first precludes it. Be wary of trying to accomplish both at the same time. There lie all the most avoidable software design pitfalls.</p>
        <p>At the end of the day, it is a professional's job to understand all of his tools throughout the software stack. Those who even reach that ballpark find few reasons to abandon any of the tools they've learned, and when they do it's usually because they're the ones building tools rather than simply using them (which is generally a pretty uncommon scenario), or in favor of an equivalent alternative that patches architectural weaknesses - certainly not just because sometimes they can.</p>
      </div>
    </article>
    <article id="p8">
      <div className="timestamp">27/02/2016 &middot;</div>
      <h2>Moxie Marlinspike</h2>
      <div>
        <p>During my lunchtime videos I watched a video by <a href="https://vimeo.com/124887048">Moxie Marlinspike.</a>
        </p>
        <p>It was really interesting and has led me to learning about Tor, Orfox, Tails and security in general.</p>
        <p>It was also nice to hear in the news that WhatsApp have introduced his end-to-end encryption to all of their users. If only I trusted them, or had proof, that they had implemented it correctly.</p>
      </div>
    </article>
    <article id="p7">
      <div className="timestamp">20/02/2016 &middot;</div>
      <h2>Lunchtime Videos</h2>
      <div>
        <p>I really enjoyed these videos by <a href="https://vimeo.com/webdirections">Web Directions</a>
        </p>
        <p>Some of the best being <a href="https://vimeo.com/148311537">Kitt Hodsden's presentation on automation</a> and <a href="https://vimeo.com/147806023">Maciej Ceglowski's talk on Website Obesity</a>
        </p>
      </div>
    </article>
    <article id="p6">
      <div className="timestamp">10/02/2016 &middot;</div>
      <h2>Tutorial: Using Json in Jade</h2>
      <div><a href="jade.html">I've moved this post to its own page.</a>
      </div>
    </article>
    <article id="p5">
      <div className="timestamp">09/02/2016 &middot;</div>
      <h2>Fluid Fonts</h2>
      <div>
        <p>I really enjoyed a <a href="https://vimeo.com/148442386" target="_blank">video by Mike Riethmuller</a> on responsive fonts and using viewpoint units.</p>
        <p>I had previously created a mixin to interpolate between font sizes on small and large screens. It had the downside of creating a huge number of breakpoints which created large bloated css files. I hadn't thought of using vw units as I didn't realise they had cross browser support. My employer has recently dropped IE8 compatibility and so I can now use vw units in a basic way.</p>
        <p>Here is my old mixin I created. The main reason I made it was to practise creating scss mixins.</p>
      </div>
    </article>
    <article id="p4">
      <div className="timestamp">07/02/2016 &middot;</div>
      <h2>Capitalisation</h2>
      <div>
        <p>I have used a language for nearly 10 years yet I'm not sure how to write it down. Is it "Javascript" or "JavaScript"; time for <b>research!</b>
        </p>
        <p>The usual <a href="http://stackoverflow.com/questions/3989731/is-it-javascript-or-javascript" target="_blank">font of all knowledge</a> says the answer is "JavaScript" and the <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript">official docs</a> agree. </p>
        <p>I still think the capitalised S looks rather silly but never mind, knowledge is power.</p>
      </div>
    </article>
    <article id="p3">
      <div className="timestamp">07/02/2016 &middot;</div>
      <h2>Goal for 2016: Accessibility</h2>
      <div>
        <p>I want to improve the accessibility of the websites I create. I will be using HTML validators more that will help with cross browser issues, compatibility on novel devices and indirectly aid accessibility.</p>
        <p>The easiest thing I am going to do is to let go of the mouse and check that I can easily navigate the site and perform all actions using the keyboard alone.</p>
        <p>At the start of each project I need to think about where the h1 tags will go on each page. I will also need to ensure that I use at least one h2-h6 in each section. Retroactively adding these presents styling issues so I must make sure to do this during the architecture portion at the start of the process.</p>
        <p>I need to make sure all my icons (in the spritesheet) have descriptive alt tags. I will check the backend team are adding them to the CMSable images.</p>
        <p>I will only use the &lt;button&gt; tag for elements that fire of javaScript functions. Buttons will only contain either basic text or an image. Anything that links anywhere will be an &lt;a&gt; tag styled to look like a button. </p>
        <p>Background image urls need to be in the HTML so they can be accessed by the CMS. Using &lt;img&gt; tags instead of background-image will help screen readers. We are going to try having a style section in the header for CMSable stylings instead of inline styles.</p>
        <p>We should add a screen reader into our testing device list. At the very least we should run some accessibility checking during internal review. This checking should be added to our "pre UAT checklist" that I am currently compiling. </p>
        <p>TODO: Reflect on these goals in a year..</p>
      </div>
    </article>
    <article id="p2">
      <div className="timestamp">01/02/2016 &middot;</div>
      <h2>Naming things</h2>
      <div>
        <p>Naming things is notoriously difficult. Here is my thought process when I added a new element to this blog.</p>
        <p>If you scroll to the top and look on the right hand side you will see a link to all my posts. Right now it doesn't exist and I need to name it so I can create it.</p>
        <p>I only need to add a tiny bit of css to it. Probably just "float: right" on large enough screens. It will just naturally stack on small screens. I don't plan on styling the home link specifically at all so that won't need a class.</p>
        <p>My first thought was to put the class 'blog' on the body so this page is namespaced. The problem would be that I would still need a class name to select the link in the name space or use awkward css selectors to get the second link.</p>
        <p>Instead I could use BEM and just call the link "blog__right". Right describes the appearance instead of the content and isn't even true on small screens. The content will always be a link so a better name is "blog__link". My class names are always lower case for consistency. I might want to add a similar link on another page. This right aligned link is always in the header so a better, more reusable, name would be "header__link". </p>
        <p>I think that is a good name. It's clear what it is, can be used on any page and has the right level of specificity. The only downside is that the home link is also a link in the header but I'm not giving that a class so this will work fine. The name isn't perfect but I can't think of a better one. I told you naming things is hard. I will now stick with that name and get on with creating the element and the new page. At some point you have to decide the name is good enough for now and move on with the project.</p>
        <p>If I end up doing something different to what I an currently planning, for example adding more links in the header, then I will look at the new situation and refactor my choice of name.</p>
      </div>
    </article>
    <article id="p1">
      <div className="timestamp">29/01/2016 &middot;</div>
      <h2>Where should I blog?</h2>
      <div><a href="http://www.tumblr.com"><h3>Tumblr:</h3></a>
        <p>The benefit is that it is very easy to set up and has lots of built in functionality. The downside is that it is a third party so that adds the risk of the service ceasing or adverts being added. It also locks me in to continuing using their platform. The user expertise is also worse as they would be taken off this site and the branding wouldn't be identical.</p><a href="http://www.medium.com"><h3>Medium:</h3></a>
        <p>I like their simple layout and their philosophy of focusing on the words themselves instead of fancy presentation. It also has a very cool share any selected text feature which, although I could recreate would require a fair amount of testing, so I probably wouldn't bother if I built my own blog. it also has the problems of being a third party.</p><a href="http://codepen.io"><h3>Codepen:</h3></a>
        <p>It is third party but it is also the home of front end. I already have a pro account and embedding pens would be easy. I did actually decide that I would blog there. You can change the css to match this site but I realised it was going to take a lot of time to chance their complicated layout to match the minimalism here. I also quickly realised that there platform is rather feature lacking and when I read their development blog they had no posts on updates or improvements to the blog which was discouraging. I realised it would be nearly as quick to build my own blog as it would be to change all their css.</p>
        <h3>Build my own:</h3>
        <p>I already have a master Pug/Jade template and the css practically done. If and when I want additional features I will build then myself.</p>
        <p>At about ten posts I'll split this blog into multiple pages. I could use Mustache.js to ajax in more posts for infinite scrolling. A previous comments section could be included but at the moment I don't want this to be a two way dialogue. Discussion could be kept to another social medium with links to the blog instead of embedding it here. </p>
        <p>There are other advantages of building my own blog. The hosting lasts as long as the website and I know it'll be advert free. The styling perfectly matches the website which creates a consistent and pleasant UX. I will have complete control over which features I add and best of all I will learn how to create them.</p>
        <p>As this blog grows I will have to think about categories, tags, overview pages and maybe adding in a search feature. The only thing I going to miss from codepen is writing in markup but as each post will be mainly just paragraphs of text and I will be using Pug/Jade it should be fine.</p>
      </div>
    </article>
    <article id="p0">
      <div className="timestamp">20/12/2015 &middot;</div>
      <h2>End of year musings</h2>
      <div>
        <h3>Optimising: Minimising http requests</h3>
        <p>We already use client dependency to concatenate all our javascript files into one file and all the css into one file. The main problems were the icon images.</p>
        <p>To solve this I added a plugin to the grunt file. It takes all the images in the "icons" folder and combines them into one "spritesheet". It then optimises the resulting png to reduce file size.</p>
        <p>Now instead of each icon needing an http request they all only require one request. The total size of them is also slightly reduced. This should reduce content and script blocking on page load.</p>
        <h3>Grunt Automation Choices - When to optimise?</h3>
        <p>All the CSS needs to be concatenated and minified for production. This doesn't take very long and so could be done in dev too. The advantage is that then the release build is the same as the dev environment, minimising additional bugs. I have chosen not to do this. During dev the grunt file doesn't optimise the CSS nor the javascript. This helps in debugging as the maps show you exactly which file the errors are coming from. I think this outweighs the risk of additional unseen bugs.</p>
        <h3>Print.css </h3>
        <p>In an ideal world we'd have time to create a css file so that printing the site looks nice. People are likely to print out job listings. If we could make this, or have a standardised one, or find a standardised one then I would like to add this to the build process. Css-tricks has a good article.</p>
      </div>
    </article>
  </Container>

  );
};

export default Blog;
