export interface DataInterface {
  name: string;
  dependencies: DataInterface[];
  key: string;
  type?: string;
}


let objectCount = 0;
let currentDepth = 0;

const createBigNestedObject = (max: number, amountOfDepth: string):DataInterface => {
  console.log('amountOfDepth', amountOfDepth);
  
  const root: DataInterface = {
    dependencies: [],
    key: "root",
    name: "root"
  }

  const addChildren = (object: DataInterface) => {
    objectCount ++;
    if (objectCount > max) {
      return
    }
  
    currentDepth++;

    let maxDepth = 1;

    if (amountOfDepth === "medium"){
      let n = 1;

      while ( n + n*n < 2*max) {
        n++
      }

      maxDepth = n - currentDepth;
    }

    if (amountOfDepth === "shallow"){
      maxDepth = max
    }
    
  
    for (let i = 0; i <= maxDepth && i + currentDepth < max; i++) {
      objectCount ++;
      object.dependencies.push({
        dependencies: [],
        key: "key " + objectCount,
        name: "test " + objectCount
      });
    }
  
    object.dependencies.forEach( (child:DataInterface) => {
      addChildren(child);
    })
  };

  addChildren(root);

  return root
};



const nestedObjectToArray = (nestedObject: DataInterface) => {
  const returnArray: any = [];

  const addRow = (row2: DataInterface, depth: number) => {
    returnArray.push({
      depth,
      formattedDepth: `${depth * 30}px`,
      key: returnArray.length,
      name: row2.name,
      type: row2.type
    });
    row2.dependencies.forEach(childRow => {
      addRow(childRow, depth + 1);
    });
  };

  addRow(nestedObject, 0);
  return returnArray;
};

export const createData = (max:number, amountOfDepth: string) => {
  objectCount = 0;
  currentDepth = 0;
  const bigNestedObject = createBigNestedObject(max, amountOfDepth);
  const bigArray = nestedObjectToArray(bigNestedObject);
  console.log("objectCount", objectCount);
  return {
    bigArray,
    bigNestedObject,
  }
}