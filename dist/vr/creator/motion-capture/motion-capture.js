AFRAME.registerComponent('avatar-recorder', {
  schema: {
    minInternal: {type: 'number', default: 50}
  },
  init: function () {
    const self = this;
    this.isRecording = false;

    this.leftHandEl = this.el.sceneEl.querySelector("#leftHand");
    this.rightHandEl = this.el.sceneEl.querySelector("#rightHand");
    this.cameraEl = this.el.sceneEl.camera;

    this.recording = {
      minInternal: this.data.minInternal,
      leftHand: {
        positions: [],
        rotations: [],
      },
      rightHand: {
        positions: [],
        rotations: [],
      },
      camera: {
        positions: [],
        rotations: [],
      }
    }

    this.previousTime = 0;
    
    this.recordKeyframe = function (key, element) {
      this.recording[key].positions.push(AFRAME.utils.coordinates.stringify(element.getAttribute('position')));
      this.recording[key].rotations.push(AFRAME.utils.coordinates.stringify(element.getAttribute('rotation')));
    };

    this.recordKeyframes = function () {
      this.recordKeyframe("leftHand", this.leftHandEl);
      this.recordKeyframe("rightHand", this.rightHandEl);
      this.recordKeyframe("camera", this.cameraEl.el);
    };

    this.eventHandler = function(event) {
      if (event.code === 'Space') {
        if(self.isRecording){
          console.log('Stop recording' )
          localStorage.setItem("motion-capture", JSON.stringify(self.recording));
          self.isRecording = false;
        } else {
          console.log('Start recording')
          self.isRecording = true;
        }
      }
    };
    document.addEventListener('keyup', this.eventHandler)

  },
  tick: function (time, timeDelta) {
    if(this.isRecording){
      if(time - this.previousTime > this.data.minInternal) {
        this.recordKeyframes()
        this.previousTime = time;
      }
    }
  }
});