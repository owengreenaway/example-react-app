import axios, { AxiosResponse } from "axios";
import { injectGlobal } from "emotion";
import * as React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Blog from "../components/legacy-html/Blog";
import Home from "../components/legacy-html/Home";
import PugTutorial from "../components/legacy-html/PugTutorial";
import Python from "../components/legacy-html/Python";
import StyleGuide from "../components/legacy-html/StyleGuide";
import UnitTesting from "../components/unit-testing/UnitTesting";
import Example from "../ui/tree/Tree";
import "./App.css";
import PerformanceTesting from "../components/performance-testing/PerformanceTesting";

// tslint:disable-next-line:no-unused-expression
injectGlobal`
* {
  box-sizing: border-box;
}

body {
  font-family: "Tahoma", sans-serif;
  font-weight: 200;
  color: #222;
  font-size: 100%;

  @media (min-width: 768px) {
    font-size: 1.25rem;
  }
}

p,
h1,
h2,
h3,
h4 {
    font-size: inherit;
    line-height: 1em;
    font-weight: 200;
    margin: 1rem 0;
}
p {
    line-height: 1.3em;
}
a {
    text-decoration: none;
    color: rgb(71, 71, 241);
}
a:hover,
a:focus {
    text-decoration: underline;
}
`;

class App extends React.Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route path="/blog" component={Blog} />
          <Route path="/styleguide" component={StyleGuide} />
          <Route path="/pug" component={PugTutorial} />
          <Route path="/python" component={Python} />
          <Route path="/unit-testing" component={UnitTesting} />
          <Route path="/performance-testing" component={PerformanceTesting} />
          <Route component={Home} />
        </Switch>
      </Router>
    );
  }
}

export default App;
