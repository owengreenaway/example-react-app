// Given the HelloWorld component then the message is rendered
// Given the HelloWorld component when there are children then the children are rendered
// Given the HelloWorld component when there are children then it matches the snapshot
// Given the HelloWorld component when there are no children then the children are not rendered
// Given the HelloWorld component when there are no children then it matches the snapshot
