import * as React from "react";
import { Link } from "react-router-dom";
import ScrollToTopOnMount from "../../app/ScrollToTopOnMount";
import Container from "../../ui/container/Container";
import { List } from "../../ui/list/List";
import { Subtitle } from "../../ui/subtitle/Subtitle";
import { Title } from "../../ui/title/Title";

const Home = ({}) => {
  return (
    <Container>
      <ScrollToTopOnMount />
      <section id="personal">
        <Title copy="Personal Projects" />

        <Subtitle copy="2020" />

        <List>
          <li>
            <a
              href="https://bitbucket.org/owengreenaway/immersive-cms/src/master/README.md"
              target="_blank"
              rel="noopener noreferrer"
            >
              A virtual reality content management system
            </a>
          </li>
        </List>

        <Subtitle copy="2019" />

        <List>
          <li>
            <a
              href="https://github.com/emilygreenaway/juggling-simulator/blob/master/README.md"
              target="_blank"
              rel="noopener noreferrer"
            >
              A virtual reality juggling simulator made with Unity and C#
            </a>
          </li>
          <li>
            <a
              href="https://www.youtube.com/watch?v=88Ebw-ihl78"
              target="_blank"
              rel="noopener noreferrer"
            >
              A virtual reality FPS with a throwing mechanic. Made with Unity
              and C#
            </a>
          </li>
          <li>
            <a
              href="https://twitter.com/owengreenaway"
              target="_blank"
              rel="noopener noreferrer"
            >
              Tweets about analysing, designing and creating virtual reality
              experiences
            </a>
          </li>
        </List>

        <Subtitle copy="2018" />

        <List>
          <li>
            <a
              href="https://bitbucket.org/owengreenaway/clothes"
              target="_blank"
              rel="noopener noreferrer"
            >
              Restful API using Node, Express and Firebase cloud functions
            </a>
          </li>
          <li>
            <Link to="/performance-testing">
              Researching React performance for long lists
            </Link>
          </li>
          <li>
            <Link to="/unit-testing">
              Created unit testing helper functions using TypeScript generics
            </Link>
          </li>
          <li>
            <a
              href="https://www.npmjs.com/package/owen-react-library"
              target="_blank"
              rel="noopener noreferrer"
            >
              Created a React &amp; TypeScript component library
            </a>
          </li>
          <li>
            <a
              href="https://bitbucket.org/owengreenaway/example-react-app"
              target="_blank"
              rel="noopener noreferrer"
            >
              This portfolio website, created with React using continuous
              deployment
            </a>
          </li>
          <li>
            <a
              href="https://bitbucket.org/owengreenaway/learning-webpack-4"
              target="_blank"
              rel="noopener noreferrer"
            >
              Experiments with webpack 4
            </a>
          </li>
          <li>
            <a
              href="https://bitbucket.org/owengreenaway/mock-api-using-service-worker"
              target="_blank"
              rel="noopener noreferrer"
            >
              Mocking API calls with a service worker
            </a>
          </li>
          <li>
            <a
              href="https://bitbucket.org/owengreenaway/angular"
              target="_blank"
              rel="noopener noreferrer"
            >
              Experimenting with Angular
            </a>
          </li>
        </List>

        <Subtitle copy="2017" />
        <List>
          <li>
            <a
              href="https://bitbucket.org/owengreenaway/card-game/"
              rel="noopener noreferrer"
            >
              "Gwent" multiplayer card game clone using React.js and Firebase
            </a>
          </li>
          <li>
            <Link to="/blog">Micro-blog</Link>
          </li>
          <li>
            <Link to="/styleguide">Style guide</Link>
          </li>
        </List>

        <Subtitle copy="2016" />

        <List>
          <li>
            <Link to="/pug">Using Json in Jade/Pug tutorial</Link>
          </li>
          <li>10k Apart Competition Entry using SCSS &amp; Jade/Pug</li>
        </List>

        <Subtitle copy="2015" />
        <List>
          <li>
            <Link to="/python">Python Object Orientated Programming</Link>
          </li>
        </List>

        <Subtitle copy="2013" />
        <List>
          <li>
            <a
              href="https://creatingjarvis.wordpress.com/"
              rel="noopener noreferrer"
            >
              Speech Recognition Macros
            </a>
          </li>
        </List>
      </section>
    </Container>
  );
};

export default Home;
