import * as React from "react";

export default class BaseTree extends React.Component<any, {}> {
  render(): JSX.Element {
    const { dependencies, row, Indent } = this.props;
    return (
      <>
        {row(this.props)}
        <Indent>
          {dependencies.map((child: any) => (
            <BaseTree {...child} row={row} Indent={Indent} />
          ))}
        </Indent>
      </>
    );
  }
}
