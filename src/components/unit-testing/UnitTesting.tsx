import * as React from "react";
import { Link } from "react-router-dom";
import ScrollToTopOnMount from "../../app/ScrollToTopOnMount";
import Container from "../../ui/container/Container";

const UnitTesting: React.SFC = () => {
  return (
    <Container>
      <ScrollToTopOnMount />
      <header className="header">
        <Link to="/">
          <h2>Home</h2>
        </Link>
      </header>

      <p>
        Most Jest tutorials only show you how to write one unit test. At most
        they show how to write one suite.
      </p>
      <p>
        I felt there was a lot of duplicated code in my unit tests and wanted to
        create some helper setup functions to solve this problem.
      </p>
      <p>Below are the files I showed to my team when I presented the idea.</p>

      <ul>
        <li>
          <a
            target="_blank"
            rel="noopener noreferrer"
            href="https://bitbucket.org/owengreenaway/example-react-app/src/master/src/components/unit-testing/documentation/1 - names.tsx"
          >
            1 - names.tsx
          </a>
        </li>
        <li>
          <a
            target="_blank"
            rel="noopener noreferrer"
            href="https://bitbucket.org/owengreenaway/example-react-app/src/master/src/components/unit-testing/documentation/2 - tutorials.tsx"
          >
            2 - tutorials.tsx
          </a>
        </li>
        <li>
          <a
            target="_blank"
            rel="noopener noreferrer"
            href="https://bitbucket.org/owengreenaway/example-react-app/src/master/src/components/unit-testing/documentation/3 - setup function idea.txt"
          >
            3 - setup function idea.txt
          </a>
        </li>
        <li>
          <a
            target="_blank"
            rel="noopener noreferrer"
            href="https://bitbucket.org/owengreenaway/example-react-app/src/master/src/components/unit-testing/documentation/4 - setup function.tsx"
          >
            4 - setup function.tsx
          </a>
        </li>
        <li>
          <a
            target="_blank"
            rel="noopener noreferrer"
            href="https://bitbucket.org/owengreenaway/example-react-app/src/master/src/components/unit-testing/documentation/5 - setup function typed.tsx"
          >
            5 - setup function typed.tsx
          </a>
        </li>
        <li>
          <a
            target="_blank"
            rel="noopener noreferrer"
            href="https://bitbucket.org/owengreenaway/example-react-app/src/master/src/components/unit-testing/documentation/6 - snapshot.tsx"
          >
            6 - snapshot.tsx
          </a>
        </li>
        <li>
          <a
            target="_blank"
            rel="noopener noreferrer"
            href="https://bitbucket.org/owengreenaway/example-react-app/src/master/src/components/unit-testing/documentation/7 - find function.tsx"
          >
            7 - find function.tsx
          </a>
        </li>
        <li>
          <a
            target="_blank"
            rel="noopener noreferrer"
            href="https://bitbucket.org/owengreenaway/example-react-app/src/master/src/components/unit-testing/documentation/8 - findById function.tsx"
          >
            8 - findById function.tsx
          </a>
        </li>
        <li>
          <a
            target="_blank"
            rel="noopener noreferrer"
            href="https://bitbucket.org/owengreenaway/example-react-app/src/master/src/components/unit-testing/documentation/9 - createSetup function idea.txt"
          >
            9 - createSetup function idea.txt
          </a>
        </li>
        <li>
          <a
            target="_blank"
            rel="noopener noreferrer"
            href="https://bitbucket.org/owengreenaway/example-react-app/src/master/src/components/unit-testing/documentation/10 - createSetup function.tsx"
          >
            10 - createSetup function.tsx
          </a>
        </li>
        <li>
          <a
            target="_blank"
            rel="noopener noreferrer"
            href="https://bitbucket.org/owengreenaway/example-react-app/src/master/src/components/unit-testing/documentation/11 - createSetup function typed.txt"
          >
            11 - createSetup function typed.txt
          </a>
        </li>
        <li>
          <a
            target="_blank"
            rel="noopener noreferrer"
            href="https://bitbucket.org/owengreenaway/example-react-app/src/master/utils/testUtils.tsx"
          >
            12 - the finished testUtils
          </a>
        </li>
        <li>
          <a
            target="_blank"
            rel="noopener noreferrer"
            href="https://bitbucket.org/owengreenaway/example-react-app/src/master/src/ui/hello-world/HelloWorld.test.tsx"
          >
            13 - example usage
          </a>
        </li>
      </ul>
    </Container>
  );
};

export default UnitTesting;
