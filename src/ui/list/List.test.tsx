import { shallow } from "enzyme";
import * as React from "react";
import * as renderer from "react-test-renderer";
import { List } from "./List";

const testChild = <li>child</li>;

const setup = (props?: any) => {
  const defaultPropsOfSetup = {
    children: testChild
  };
  props = Object.assign(defaultPropsOfSetup, props);
  return {
    snapshot: renderer.create(<List children={props.children} />).toJSON(),
    wrapper: shallow(<List children={props.children} />)
  };
};

describe("Given the List component", () => {
  describe("when given children", () => {
    it("then it renders the children", () => {
      const { wrapper } = setup();
      expect(wrapper.contains(testChild)).toBeTruthy();
    });
    it("then it matches the snapshot", () => {
      const { snapshot } = setup();
      expect(snapshot).toMatchSnapshot();
    });
  });
});
