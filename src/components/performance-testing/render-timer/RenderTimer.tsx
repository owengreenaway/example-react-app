import * as React from "react";

interface RenderTimerProps {
  startTime: Date;
}

interface RenderTimerState {
  renderDuration: number;
}

export class RenderTimer extends React.Component<
  RenderTimerProps,
  RenderTimerState
> {
  constructor(props: any) {
    super(props);
    this.state = {
      renderDuration: 0
    };
  }

  componentDidMount() {
    const now = new Date();
    const timeInMilliSeconds = now.getTime() - this.props.startTime.getTime();
    this.setState({ renderDuration: timeInMilliSeconds });
  }

  render() {
    return <div>Rendered in {this.state.renderDuration}ms</div>;
  }
}
