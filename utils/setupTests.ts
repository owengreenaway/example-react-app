import * as emotion from 'emotion'
import * as Enzyme from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';
import { createSerializer } from 'jest-emotion';

Enzyme.configure({ adapter: new Adapter() });
expect.addSnapshotSerializer(createSerializer(emotion))