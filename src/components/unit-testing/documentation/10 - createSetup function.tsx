import { shallow, ShallowWrapper } from "enzyme";
import * as React from "react";
import * as renderer from "react-test-renderer";
import HelloWorld from "../../../ui/hello-world/HelloWorld";

export const createSetup = function(Component: any, requiredProps: any) {
  return (propOverrides?: any) => {
    const finalProps = Object.assign(requiredProps, propOverrides);
    return {
      props: finalProps,
      wrapper: shallow(<Component {...finalProps} />) // must start with a capital letter
    };
  };
};

export const createSnapshot = function(Component: any, requiredProps: any) {
  return (props?: any): renderer.ReactTestRendererJSON => {
    const finalProps = Object.assign(requiredProps, props);
    return renderer.create(<Component {...finalProps} />).toJSON();
  };
};

export type ShallowWrapperFunction = (
  enzymeWrapper: ShallowWrapper<any, any>
) => ShallowWrapper<any>;

export const findById = (
  testingId: string
): ShallowWrapperFunction => enzymeWrapper =>
  enzymeWrapper.find(`[data-testing-id="${testingId}"]`);

const findChildren = findById("hello-world-children");
const findMessage = findById("hello-world-message");

const defaultProps = {
  message: "test-string"
};

const testChild = <p>I am the child</p>;

const setup = createSetup(HelloWorld, defaultProps);
const snapshot = createSnapshot(HelloWorld, defaultProps);

describe("Given the HelloWorld component", () => {
  it("then the message is rendered", () => {
    const { wrapper, props } = setup();
    expect(findMessage(wrapper).props().children).toBe(props.message);
  });

  describe("when there are children", () => {
    it("then the children are rendered", () => {
      const { wrapper } = setup({ children: testChild });

      expect(findChildren(wrapper).exists()).toBe(true);
      expect(findChildren(wrapper).contains(testChild)).toBe(true);
    });

    it("then it matches the snapshot", () => {
      expect(snapshot({ children: testChild })).toMatchSnapshot();
    });
  });

  describe("when there are no children", () => {
    it("then the children are not rendered", () => {
      const { wrapper } = setup({ children: undefined });
      expect(findChildren(wrapper).exists()).toBe(false);
    });

    it("then it matches the snapshot", () => {
      expect(snapshot()).toMatchSnapshot();
    });
  });
});
